# まず始めに

## ModuleをCoolingBoxへの設置 <br>
moduleのテストを行うために、まずCoolingBoxに設置します。 <br>

## 環境変数の登録 (INITIAL_WARM/INITIAL_COLD) <br>
モジュールをCoolingBoxに設置したら環境変数の設定を行います。 <br> 
### **INTIAL_WARM INITIAL_COLDそれぞれのステージで必要**

    1. モジュールをCooloingBoxに設置後、解析ディレクトリーに移動.

```
$cd /nas/daq/QCtest/module-qc-operator
```

2. 解析ディレクトリー上にあるsetup.shを実行

```
$ ./setup.sh -c configs/setup/repicdaq1_setup.json <br>　
# json fileはDAQ毎に分けられている DAQ1 : repicdaq1, DAQ2 : repicdaq2, DAQ3: repicdaq3
> Do you want to change env setting ? (y/n)
y
Input module SN (default:20UPGM22601029 ) : 
> set moduleSN : 20UPGM22601029
Config Type :(default :L2_warm)
0 : L2_warm
1 : L2_cold
>set config type : L2_warm

```
ここで引数-c config/setup/repicdaq1.jsonはDAQのhostnameに対応したものをそれぞれ選択。 <br>  
実行後環境設定を変更するか聞かれるのでyを選択しmoudleSN,config typeをそれぞれ選択。  <br>


config typeは２種類あり20度でのElectrical testを warm test (L2_warm), -15度でのtestをcold test (L2_cold)となっている。  
最初は warm testなので 0:L2_warmを選択。  

* 環境設定は env/.repicdaq1_envに書き換えられる(環境設定の詳細を変更する場合は直接env fileを修正するとよい)。  

## config fileのダウンロード

次にlocalDBからconfig fileをダウンロードを行う。  
*Config fileは強制上書きされるので注意  
config fileは /nas/daq/QCtest/moduleディレクトリー上のmoudleSNに保存されるので要確認

```
$./ConfigDownload.sh 
======== Defolt Env Setting =============
moduleSN     : 20UPGM22601029 <- moduleSNが正しいか確認!
Config Type  : L2_warm
INSTITUITON  : HR <- HR :ハヤシレピック
LocalDB_HOST : 192.168.100.104 
LocalDB_PORT : 5000
HV Setting   : -80 [V]
=========================================
Config Type
0  all (L2_warm + L2_cold + L2_LP)
1  L2_warm
2  L2_cold
3  L2_LP
#ダウンロードするconfig fileを指定(default : 0 )
$0
Input accessCode1: #ATLAS PDBのaccessCode1を代入
Input accessCode2: #ATLAS PDBのaccessCode2を代入
```

ダウンロードが完了したら/nas/daq/QCtest/moduleディレクトリー上にmoudleSNに保存されいるか確認
