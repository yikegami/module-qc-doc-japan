# 読み出しのデバッグ

## CML Biasのデフォルト値の変更

典型的には，Chip2などで，digitalscanを走らせたときにOccupancy Mapに規則的なパターンが生じる．これは主にデータ通信時のデータ信号の劣化でデータ読み取りに起因するデータの欠損が起こるためである．

![Example1](images/CmlBadPatternExample1.png)

館山のシステムでは，多くの場合`CmlBias1`のパラメータを`200`程度に設定することで改善することが知られているが，よりシステマティックに行うためには`CmlBiasScan`を走らせると良い．

CML Biasのデフォルト値を変更するには，

```
$ ./lib/ChangeCmlBias.sh [chipid] [BiasChannel [0,1,2]]  [NewValue]
```

を行います．`CmlBias1`を`200`に設定する場合は，

```
$ ./lib/ChangeCmlBias.sh all 1 200
```

とします．chip毎に個別したい場合は 一つ目の引数にchip番号を使います。(例、chip1の `CmlBias1`を`200`にする)

```
$ ./lib/ChangeCmlBias.sh 1 1 200
```

`2023-08-23`現在，`CmlBias1`のHRでの推奨値は`200`とします（初期値：`400`）．
→ First digital scanの結果が不良の場合にはCmlBias1の値を`100`程度まで下げて改善するか見てください．



## CoreColumnのマスク

不良（ノイジー）なピクセルが存在すると，そのピクセルが悪影響をしてデータが（部分的にしか）送られてこなかったり，もっと悪い場合にはConfig自体が通らなかったりします．このとき，該当するピクセルを含むCoreColumnをうまくマスクすることができれば，ノイジーなピクセルがミュートされ，テストがうまくいく可能性があります．

!!!note
	CoreColumnのマスクは`8x384 = 3072`ピクセルをいっぺんにマスクするので，全モジュールの約0.5%，そのFEの2%のピクセルを失います．Pixel Failure Analysisを行うとBad Pixelとして数えられてしまうので，QCをpassする可能性が低まります．極力CoreColumnをマスクせずにQCが行えるのが望ましいです．

## chip の Enable/Desable の変更

YARRのスキャン等で、1チップだけのスキャンを行いたい。とか、悪いチップをDisableしたいといったときは以下のコマンドを使ってください。

```
$ ./lib/ModifyChipEnable.sh [status]
```

[status]は、4chipに相当する4桁の1/0の文字列で指定できます。1... enbale 0...disable 
例えば、chip 2だけdisableしたい場合は、

```
$ ./lib/ModifyChipEnable.sh 1011
```

で可能です。
（エディタで直接connectivity configをいじることは非推奨とします．）

## First Digital Scan

とりあえずModuleとのデータ通信の健全性を確認したいときは，`digitalscan`を走らせて通信に問題がないか確認します．

```
$./lib/Scan.sh -m 0 -W TEST
```
