# 熱サイクル試験の手順

## サーマルサイクルの始め方
サーマルサイクルの手順としては、「前回のモジュールの取り外し」→「モジュールのセッティング」→「サイクルの開始」→「モジュールの取り外し」というサイクルで運用される。
ほとんどの場合、前回のサイクルのモジュールの取り外しから始まるので、ここではそれに即した順に解説が並んでいる。
!!! warning
    吸着ポンプは真空が維持できていない状態でONだと発熱する。モジュールが設置されていない間はポンプに繋がるバルブはしっかりと閉め、長期間設置しない場合はポンプをOFFにすること。

### モジュールの取り外し
!!! note
    次にサーマルサイクルを回すモジュールを先に決めて、そちらの準備も済ませてから開始すると効率的。
1. Grafana上で恒温槽温度が室温に戻り、恒温槽がSTANDBYであることを確認
2. 真空ポンプ・乾燥空気切り替えバルブを以下のようにセットし、真空吸着を切り、乾燥空気の正圧を加える。上から順の操作を推奨。「–」の時ON、「｜」の時OFF。
!!! note
    操作順番は真空ポンプとモジュール取り出し用乾燥空気が同時にONになる状況を避ける。  
   <img src="../ThermalCyclingPics/IMG_0364.jpg" width = 50%>  
3. ブラウザで192.168.100.108:8089/home/に接続し"Module Removal"ボタンを押す(以下の画像のように画面遷移したら次のステップへ)  
   <img src="../ThermalCyclingPics/removal.png" width = 50%>  
4. 恒温槽温度が40℃になるまで待ち、恒温槽がSTANDBY(OFF)になったのを確認したら、ボトムプレートとねじ、適宜足場を準備し、扉を開ける。  
!!! note
    モジュールをどの向きに戻すかこのタイミングで確認しておくと良い。  
5. 適宜ステージを引き出し、恒温槽内でNTC読み出しコネクタのQIコネクタを外し、モジュールを取り出してボトムプレートに戻す。  
!!! warning
    持ち上げる際にモジュールが吸着ヘッドに張り付いていることがある。慎重に持ち上げ、タグに負荷がかかっているのを感じた場合は無理に上方に持ち上げない。そのような場合は、少し傾きをつつけて、手前側から持ち上げるようにすると外れやすい。  
6. NTCコネクタを外す。状況が許す限り次は写真撮影なので、トッププレートを被せた状態で四隅のねじを外し、写真撮影台にマウントできるようにする → IV撮影へ  
!!! note
    NTCコネクタを外す際は、ピックアップポイントなど、抑えても大丈夫な場所を確認したうえで抑えつつ、すこしずつ左右に傾けるように抜くと抜けやすい。    
7. 恒温槽の扉を閉め、ブラウザで192.168.100.108:8089/home/に接続し"Removal Done"ボタンを押す。  
8. 操作禁止のプレートを裏にし、ホワイトボードをクリアする。  
!!! note
    足場を片付けるのも忘れずに。  
9. 恒温槽が室温に戻り、恒温槽の操作は終了。
10. local DBのモジュールのページで、Thermal CyclingのManual Inputを選び、以下の図のように数値を埋めてProceedする。
   <img src="../ThermalCyclingPics/TC_localDB_image.png" width = 100%>  


### モジュールのセッティング
1. モジュールがキャリアから出ている場合は、まずキャリアに格納する。
!!! note
    必要に応じてモジュール保護用のカバーを用いて操作する。  
2. ボトムプレートとトッププレートのみが外せるように、ねじをつけはずしする。
3. トッププレートを取り外す。接続されている場合はピッグテールも取り外す。
4. NTC読みだしコネクタをモジュールに接続する。
!!! warning
    コネクターは図のようにモジュールのソケットの下の段に接続し(コネクタの中央から向かって外側の段)、向きは画像の通りに接続することに注意する  
!!! warning
    コネクタは前後方向に負荷をかけるとピンが曲がるか危険性があるので注意する  

   <img src="../ThermalCyclingPics/TC_image-crop.png" width = 50%>

5. Grafana上で"Chamber Status"が"Standby"であり、恒温槽内部の温度が室温であることを確認してから、恒温槽の扉を開け、モジュールを載せる吸着ヘッドが乗っているステージを慎重に引き出す。  
6. 必要に応じて足場を用意してから、ボトムプレートからモジュールをトップフレームごと取り出し吸着ヘッドに装着する。  
7. NTC読み出しコネクタのQIコネクタを恒温槽内部のコネクタに接続する。  
8. 正しく接続できていればgrafanaでモジュールの温度が確認できる。温度が読めていない場合は接続が間違っている可能性が濃厚なので、再接続するなどして確認する。再接続しても温度が読めないようならエキスパートに報告する。
   <img src="../ThermalCyclingPics/moduleNTC_grafana.png" width = 50%>
9. 引き出していたステージを元の位置まで押し込む。  
10. 使用するチャンネルの真空吸着をバルブを捻ってONにする。  
   バルブの向きが「－」の時真空吸着ON、「|」の時OFF  
   <img src="../ThermalCyclingPics/IMG_6367.jpg" width = 50%>  
11. 恒温槽側面の真空ポンプ・乾燥空気切り替えバルブを以下のようにする。  
   上から真空ポンプ・恒温槽内用乾燥空気・モジュール取り出し用乾燥空気の切り替えを行う。下から順の操作を推奨。  
!!! note
    操作順番は真空ポンプとモジュール取り出し用乾燥空気が同時にONになる状況を避ける。  
   <img src="../ThermalCyclingPics/IMG_0366.jpg" width = 50%>  
12. 負圧はモジュールの設置位置が不安定だと正常に維持できないので、表示が図のようになっていない場合は、モジュールの置き方を確認する。
   <img src="../ThermalCyclingPics/TC_pressure.png" width = 50%>
!!! note
    まれなケースだが、どこかに空気漏れがあって負圧が維持できていない状態だと、キャリア周辺から空気漏れの音がする。この音が消えるようにモジュールの位置を調整すると良い。
13. 恒温槽の扉をしっかりと閉め、ロックをかける。  
!!! warning
    ハンドルはしっかり押し込まないとロックされないので注意。扉側面下方のロックがかかっていることを視認すると確実。  
   <img src="../ThermalCyclingPics/image1.png" width = 70%>
   <img src="../ThermalCyclingPics/IMG_0363.jpg" width = 50%>  
14. ボトムプレートやトッププレートなどは恒温槽横のところに置く。  
   <img src="../ThermalCyclingPics/IMG_0725.JPG" width = 50%>  
15. マウントしたモジュールのシリアルナンバーと時刻をホワイトボードに書き、操作禁止のプレートを表にする。  
!!! note
    足場を片付けるのも忘れずに。  

### サイクルの開始
1. ブラウザで192.168.100.108:8089/home/に接続  
2. Start Thermal Cycleをクリック  
3. フォームを満たして使用するチャンネルのテキストボックスにモジュールIDを入力し、SubmitをクリックするとTCスタート  
   <img src="../ThermalCyclingPics/スクリーンショット_2023-11-13_16.16.20.png" width = 50%>  
4. Result画面が表示され、Homeボタンを押すと元の画面に戻る。  
5. Grafana 上でTC StatusがONGOINGであることを確認  
   <img src="../ThermalCyclingPics/スクリーンショット 2023-10-05 22.28.58.png" width = 50%>  
   <img src="../ThermalCyclingPics/スクリーンショット 2023-10-06 9.34.02.png" width = 50%>  


## TC Form info
サーマルサイクルはRegular Cycle + Last Cycle で構成される  
- **Machine :** マシン名を記入(REPICなど)  
- **Number of Thermal Cycle :** 標準サイクルの回数を記入  
- **Min Regular Temp :** Regular Cycleの最低温度を記入  
- **Max Regular Temp :** Regular Cycleの最高温度を記入  
- **Min Last Temp :** Last Cycleの最低温度を記入  
- **Max Last Temp :** Last Cycleの最高温度を記入  
- **Use Last Cycle :** チェックするとLast Cycleも実行される  
- **Channel Select :** 使用するチャンネルのみチェックしテキストボックスにはモジュールのIDを入力  

## Initial Check
TCが始まるとGrafana上で確認可能  
<img src="../ThermalCyclingPics/Untitled4.png" width = 60%>  

TC開始前  
<img src="../ThermalCyclingPics/Untitled5.png" width = 60%>  

TC中  

TC開始後、Initialの欄が全て✅になると温度変化が始まる  

- DP check : 露点が恒温槽の温度より10℃下回っているか  

- Vacuum check : 使用するチャンネルが吸着圧力が500mbar以上であるか  

## Interlocks
以下の状態になるとインターロックがかかりサイクルは中断され室温(25度)に戻る。  
- Dewpoint : 恒温槽温度 < 露点温度 + 2度  
- Vacuum : 吸着の負圧 < 200mbar  
- Temp : 恒温槽温度 > 70度  

## TC log
WebAppのTC Log のリンクではシステムのログ閲覧と恒温槽へのコマンド送信が可能  
コマンドの使い方は[ここ](http://lampx.tugraz.at/~hadley/semi/ch9/instruments/Humidity_Chamber/Japanese/pdf/network/40001040051_J.pdf)を参照  

## System Daemons
pythonファイルは"/home/admin/Titech_thermalcycle/tc_logger/TC_scripts/"下にある.  
- tc_sht85.service  
   mon_sht85.py  
- tc_arduino.service  
   mon_arduino.py  
- tc_su262.service  
   mon_su262.py  
- tc_interlock.service  
   interlock.py  
- TC_app.service  
   /usr/bin/python3 manage.py runserver  192.168.100.108:8089  
- ExecTC.service  
   ExecThermalCycle.py  
