# モジュールVisual Inspection手順

# 写真撮影の作業

必要な写真リスト

* `ASSEMBLY`: Front + Back
* `WIREBONDING` : Front
* `INITIAL_WARM` : N/A
* `INITIAL_COLD` : N/A
* `PARYLENE_MASKING` : Front + Back
* `PARYLENE_COATING` : N/A
* `PARYLENE_UNMASKING` : Front + Back
* `WIREBOND_PROTECTION` : Front
* `THERMAL_CYCLES` : Front + Back
* `FINAL_WARM` : N/A
* `FINAL_COLD` : N/A

# 撮影手順

* 「Replace」ボタンを用いて顕微鏡ステージをモジュール交換位置に移動する．
* モジュールはCooling Cellなしの場合に「１」の位置（フラット吸着）に，ありの場合に「２」（凹んだ吸着）の位置に置く．
* 裏面測定またはメトロロジーを要求するステージの場合は，モジュール本体のみを置く．その際，PCBの下にサポートするフレームを置く．位置決めピンをPCBフレームの正面から見て「右上」と「左下」の対角の位置に挿入することで，メトロロジーの要求する位置精度で配置する．
* それ以外のステージの場合は，キャリアに格納したモジュールを置く．
* 適切なモジュールSN，ステージ，面を入力する．__この名前は後でInspectionを行うシフターが正しい画像を選ぶ際に参照するため間違えてはいけない．__


# Inspection手順
* [QNAP File Browser](https://192.168.100.101/share.cgi?ssid=7b7f9eab1e9e42159a79818747b27db8)にアクセスし，`PreProduction/Metrology/stitched/`にナビゲートし，SN，ステージ，面に対応する画像をダウンロードする．一つの撮影につき３種類の品質の画像が生成されるが，`_vi.jpg`を選択する．
* `module-qc-nonelec-gui`を用いてinspectionを行う．

## Iref trimについて

各チップのIref trimのワイヤーが正しく打たれているかをチェックし、そのワイヤー位置（4つのパッドが4ビットに対応し、ワイヤーがある = 0、ワイヤーがない = 1で数える）から読み取れるIref trim値をcheckout時のコメント欄に記入する。また、スプレッドシートの「Iref trim bit」カラムにある値と一致しているか確認する。Iref trimワイヤーがどの位置にあるかは、 https://atlas-jp-itk-pixel-module-qc.docs.cern.ch/docs/ITkPix_v3-2_BondingMap_v2023-02-15a.pdf で確認できる。

以下はIref trimワイヤー位置に対応する写真の例。赤丸位置にある4つのパットが4ビット値に対応し、この例の場合は「1101」となっているので、Iref trim値は13となる。

![Iref trimの例](images/front_tile0_IrefTrim_example_20UPGM22601025.jpeg){align=right, width=400}
