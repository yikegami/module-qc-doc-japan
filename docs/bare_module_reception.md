# Bare Module Reception Test

## IV measurement
Manuals written by Iizuka and Sato in 2022/2023

[GUI](http://atlaspc5.kek.jp/do/view/Main/FormFactorProber)

[Prober](https://gitlab.cern.ch/atlas-jp-itk/module-qc-doc-japan/-/blob/master/docs/docs/BareModule_IVProber.pdf)

## Bare module Metrology
### photo scan
Manual written by Karin Kuramochi in 2023

[Metrology machine photo scan](https://cernbox.cern.ch/s/UuS7ViL5mWRQ63V)

### Visual Inspection 
A similar way as that in PCB and modules  