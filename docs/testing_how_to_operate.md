# Simple Scan Testの使い方
IV測定後、Simple Scan Testを行います。
Simple Scan Testの実行方法は以下の通り
<img src="../images/flowchart.jpg" width="640px">

### 1. Moudle QC Operator のディレクトリーに移動 <br>
    $cd /nas/daq/QCtest/module-qc-operator <br>

### 2.  Simple Scan Testをまとめて実行 <br>
    $ ./SimpleScanOperator.sh <br>
### 3. テストが終わるまで待つ(テストが終わると以下のメッセージが出るよ！) <br>
![how_to_use_simple_scan](../images/SimpleScan_how_to_operate_1.jpg)

### 4. configのアップデート <br>
    localdbでテスト結果を確認 <br>
    問題なし -> y (アップデートが開始されるよ) <br>
    問題あり (中止) -> n <br>
![how_to_use_simpel_scan2](../images/SimpleScan_how_to_operate_1.jpg)

### 5. 次のテストに進む <br>
    configのアップデートが完了したら次のテストに進むか聞かれるよ <br>
    問題なければ  -> y  <br>　　（手順３に戻る）　
    問題あり(中止) ->n  <br>　　（テストのやり直し手順２＊）

----------------------------------------------------------------------------------------- <br>
<br>
----------------------------------------------------------------------------------------- <br>



## 　特定のSimple Scanを実行したいとき（Adance Options）
途中でテストを中断した場合やデバックをしたいときは手順2*, or 2**を進めるといいよ
困った場合はシフトリーダーに相談しよう！

### 2* SimpleScan途中から実行する場合 <br>
    途中からテストを開始したい場合、-s オプションを加える　<br>
    $./SimpleScanOperator.sh -s {i} <br> 
    ### {i} : Simple Scan Test番号  ###
    0 : ADC-CALIBRATION <br>
    1 : ANALOG-READBACK <br>
    2 : SLDO  <br>
    3 : VCAL-CALIBRATION <br>
    4 : INJECTION-CAPACITANCE <br>
    5 : LP-MODE <br>
    6 : OVERVOLTAGE-PROTECTION <br>

    手順3に戻るよ！

### 2** 特定のSimpleScanを実行したい場合 (主にdebug用) <br>
    まとめてではなく、特定のSimpleScanを実行したい場合 <br>
    $./lib/Scan.sh -m {i} <br>
    ### {i} : Simple Scan Test番号   ###
    0 : ADC-CALIBRATION <br>
    1 : ANALOG-READBACK <br>
    2 : SLDO  <br>
    3 : VCAL-CALIBRATION <br>
    4 : INJECTION-CAPACITANCE <br>
    5 : LP-MODE <br>
    6 : OVERVOLTAGE-PROTECTION <br>

    手順３に戻るよ! (次のテストに進まないので注意 手順3->手順４->終了)
