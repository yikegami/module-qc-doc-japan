# Experts On-call

On-call担当の人は自分のSkypeへの招待リンクをこのページに記入してください

当日のOn-callは[スプレッドシート](https://docs.google.com/spreadsheets/d/1fCHiRRp5fFAixouX3U1oIQ0DX0SNlsu-lbWTl--Q5zY/edit?usp=sharing)から確認できます．

## CoolingBox

* 生出：[Skypeリンク](https://join.skype.com/invite/f9zafutV2bHw)


## Database

* 生出：[Skypeリンク](https://join.skype.com/invite/f9zafutV2bHw)

## Metrology

* 中村：[Skypeリンク](https://join.skype.com/u6cZbLDk2h6z)
* Member : 中村(KEK)、倉持(筑波大)、河野(お茶大)、菅原(筑波大)
