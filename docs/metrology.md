# 組み立て後モジュールのMetrology手順

## 0. 測定時の基本的な注意事項
### 測定の順番
測定は以下の順番で行う。(2023/08/01現在)

* アセンブリ後 (ASSEMBLY)

    1. 表面の撮影 (LED off)
    2. 表面の撮影 (LED on)
    3. 表面のサイズスキャン
    4. 裏面の撮影 (LED off)

* ワイヤーボンディング後 (WIREBONDING)

  1. 表面の撮影 (LED on)

* パリレン出荷のためのマスク後 (PARYLENE_MASKING)

**[to be described]**

* パリレンから返ってきた直後 (PARYLENE_COATING)

**[to be described]**

* パリレン用のマスク剥がし後 (PARYLENE_UNMASKING)

**[to be described]**

* サーマルサイクル後 (THERMAL_CYCLES)

**[to be described]**

### モジュールの設置
表面の撮影、メトロロジーの際にはポジション1に、裏面の撮影の際にはポジション3にモジュールをセットする。

アセンブリ後モジュールの測定時には、ポジション1のステージにボトムフレームをはめ、その上にモジュールを設置する。

![ポジション1モジュールなし](images/position1_nomodule.png){align=right, width=400}
![ポジション1モジュールあり](images/position1_module.png){align=right, width=400}

ワイヤーボンディング後モジュールの測定には、ポジション1のステージにモジュールキャリアごと設置する。この際、測定対象モジュール側のモジュールキャリアのボトムフレームは取り外す。

モジュールの設置、取り外しをする際には、常に「Replace Position」で、真空吸着をoffにして行う。ステージを動かす際には、モジュールが滑るのを防ぐため、真空吸着がonの状態で行う。

### 撮影した写真の保存場所
    /nas/PreProduction/Module/[ATLAS_SN]/[STAGE]

    module-qc-nonelec-gui などで使います。

撮影された写真は、ローカルディスク (/nas/PreProduction/Module以下) と、Share Link (https://192.168.100.101/share.cgi?ssid=7b7f9eab1e9e42159a79818747b27db8) のPreProduction\Module以下の、各モジュールのシリアル番号に対応するフォルダーにアップロードされる。

## 1. 写真撮影の手順
（2023/08/01時点での手順。手順やGUIの変更などがあれば、その都度変更してください）

### アセンブリー後の測定
1. ポジション1にボトムフレームが設置されていることを確認。設置されていなければ、設置する。
2. モジュールをポジション1に設置する。
    * GUI上でVacuumがoffになっていることを再度確認。
    * まず、位置決めピンを_右上_と_左下_に挿入する。
    * モジュールを設置する。

!!! note "モノは所定の位置に！"
  キャリアはキャリア置き場に、位置決めピンはピン入れに常に置こう。

3. GUIにモジュールのシリアル番号などを入力する。

    ![Alt text](image.png)
    
    * SN欄にシリアル番号を入力.入力欄をクリックしカーソルを表示した状態で、キャリア蓋にあるQRコードをスキャンすると、自動入力。SETをクリックし、コンポーネートタイプをセット。
    * PARTS欄が自動で "Module"になっていることを確認して、すぐ下のラジオボタンで "Outer Endcap" または "Outer Barrel"を選択する。(Outer Barrelを選択するとセル付きのモジュールを測定するのでジグの左下でスキャンが行われる。)
    * Stage欄から「ASSEMBLY」を選択。
    * Type欄から「PreProduction」を選択。
    * 「Set Scan list」をクリックし必要なテストがチェックされているか確認。
    * 「Start」をクリックすると、音が鳴り自動でスキャンが進行する。
    * 音が鳴るたび作業の指示が促されるので、それに従い進めていく。

    スキャンは自動で、表面撮影（LEDなし）、表面撮影（LEDあり）、形状測定の順に進む。

!!! warning
	ASSEMBLYステージでは裏面を撮るため，吸着後， __ピンを抜く__ こと．
    これらは途中でポップアップ画面で指示があるので見逃さないように。指差し確認推奨。

4. それぞれのスキャンが終了すると、自動でスティッチングスクリプトが走り、スティッチング後の写真が別Windowsで表示される。正しく撮影できていることを確認したら「×」をクリック。「Do you want to save this data to disk server as background」と聞かれたら、「はい」と答えると、写真がShare Linkにアップロードされる。
    * もし問題があれば、「いいえ」をクリックし、Single Scanをやり直す。

表面のサイズスキャン中には、以下の写真を撮影する。治具表面とFlex pickup areaは4枚中最低3枚、Flex fiducial marksは4枚すべてにフォーカスがあっていることを確認すること（Flex fiducial marksは、まわりの黒い部分にフォーカスしてしまう場合がある。マークの構造―中心周りで4カ所空いている部分―が認識できれば問題ない）。

治具の表面（高さの基準）
![治具表面](images/ref/ref_module_jig_s.png){align=right, width=800}

Flex pickup area
![治具表面](images/ref/ref_module_flex_pickup_s.png){align=right, width=800}

Flex fiducial marks
![Flex fiducial marks](images/ref/ref_module_flex_fmark_s.png){align=right, width=800}

ASIC fiducial marks
![ASIC ucial marks](images/ref/ref_module_asic_fmark_s.png){align=right, width=800}

HV capacitor
![HV capacitor](images/ref/ref_module_hvcapacitor_s.png){align=right, width=800}

Powerコネクタ
![Powerコネクタ](images/ref/ref_module_connector_s.png){align=right, width=800}

ASIC右辺
![ASIC右辺](images/ref/ref_module_asic_right_s.png){align=right, width=800}

Sensor下辺
![Sensor下辺](images/ref/ref_module_sensor_bottom_s.png){align=right, width=800}

ASIC左辺
![ASIC左辺](images/ref/ref_module_asic_left_s.png){align=right, width=800}

Sensor上辺
![Sensor上辺](images/ref/ref_module_sensor_top_s.png){align=right, width=800}

Flex左辺
![Flex左辺](images/ref/ref_module_flex_left_s.png){align=right, width=800}

Flex上辺
![Flex上辺](images/ref/ref_module_flex_top_s.png){align=right, width=800}

Flex右辺
![Flex右辺](images/ref/ref_module_flex_right_s.png){align=right, width=800}

Flex下辺
![Flex下辺](images/ref/ref_module_flex_bottom_s.png){align=right, width=800}

(Below, ONLY for the ASSEMBLY stage)

5. 表面の測定が終了した後、裏面測定のためにモジュールをひっくり返す操作をASSEMBLYステージでは要求される。ポップアップの指示に従えば良い。以下は要約。

6. 右側の治具を左側の治具に被せるようにしてゆっくり下す。4つの真空吸着がFlex表面のpickup pointに触れていることを確認したら、真空吸着を下側の治具から上側の治具に切り替える。このとき、モジュールが上下両方で引かれないように、下記の順番で切り替える。
     * まず、下側治具の青いつまみを回して吸着を閉じる。
     * つぎに、上側治具の青いつまみを回して吸着を開ける。

![治具着陸後](images/placed.png){align=right, width=400}

!!! warning
	  Moduleが落ちる可能性があるため以下の作業は慎重に！！

  7. 上側の治具をゆっくり持ち上げ、元の位置に戻す。この時点で、モジュールは裏面が上を向いた状態でポジション3に自動的にセットされる。

![治具運搬中](images/flipped.png){align=right, width=400}

  8. ポップアップウィンドウの「はい」をクリックすると、裏面の撮影が始まる。
  9. 裏面撮影が終わったら、表面測定の手順4. を実行する。

以上で、アセンブリ直後の測定は終了。ITkPixモジュール作業票の「アセンブリ終了後」の「写真撮影（表、裏）」「測量（メトロロジー）」がすべて終了。続いて、module-qc-nonelec-guiで外観検査、メトロロジーデータ解析し、問題がないかを確認してデータベースに上げたのを確認。必要項目をチェックシートに記入しシフトリーダにチェックシートを確認してもらう。
module-qc-nonelec-guiで写真確認するときにウィンドウが複数モニターにまたがって表示されてやりにくい場合（repicのメトロロジーマシン）は、どちらかのモニターで最大化して写真確認を行うとやりやすい。
この後、REPICの方に引き渡し、ワイヤボンディングをして貰う。

![シート](images/sheet.png){align=right, width=400}

#### Outer Barrel (OB) 裏面の写真
OBモジュールのアセンブリではASIC側（裏面）に冷却用の部品まで接着する。冷却用の部品はPGT製の黒く四角い板と4本の脚のついたリング状の部品（セル）からなる。セルの4本の脚の位置を正確に測る必要があるため、その写真がちゃんと撮れていることが重要である。(ただし、１本目の写真のみピントがずれている場合は、５番目の写真を確認してピントが合っていればOK)

裏面AsicL
![裏面AsicL](images/cellback_asicL.png){align=left, width=800}

裏面AsicR
![裏面AsicR](images/cellback_asicR.png){align=left, width=800}

裏面SensorT
![裏面SensorT](images/cellback_sensorT.png){align=left, width=800}

裏面SensorB
![裏面SensorB](images/cellback_sensorB.png){align=left, width=800}

裏面PGT (ピントが合っているかだけ確認)
![裏面PGT](images/cellback_pgt.png){align=left, width=800}

裏面(10倍レンズ,LP) AsicL
![裏面LP AsicL](images/cellback_lpasicL.png){align=left, width=800}

裏面(10倍レンズ,LP) AsicL
![裏面LP AsicL](images/cellback_lpasicR.png){align=left, width=800}

裏面(10倍レンズ,LP) AsicT
![裏面LP AsicL](images/cellback_lpasicT.png){align=left, width=800}

裏面(10倍レンズ,LP) AsicB
![裏面LP AsicL](images/cellback_lpasicB.png){align=left, width=800}

裏面Locator pins
![裏面Locator pins](images/cellback_locatorpins.png){align=left, width=350}


### ワイヤーボンディング後の測定

ワイヤーボンディング後には、表面の撮影のみを行う。

1. モジュールのシリアル番号などを入力する。
    * SN欄にシリアル番号をスプレッドシートで確認し、入力。
    * Stage欄から「WIREBONDING」を選択。
    * Type欄から「PreProduction」を選択。
   これで「Start」と書かれた緑丸をクリックすると、ステージが「Replace Position」に引き出され、真空吸着がoffになった上で、「Please set module with backside up」というメッセージが表示される。
2. ポジション1にボトムフレームが設置されていたら、それを取り外す。
3. モジュールをポジション1に設置する。
    * まず、モジュールキャリアのボトムプレートを外し、トッププレートはネジだけを外す。
    * モジュールをキャリアごとステージのポジション1に設置し、トッププレートを取り外す。
   ここまでできたら、ポップアップウィンドウの「はい」をクリックすると、スキャンが始まる。
4. アセンブリ直後の測定の手順4.　を実行する。

## 2. メトロロジー解析の手順

### 解析のための環境設定とGUI起動

1. metrology machineの常駐プログラムリストなどで、Xmingが起動していることを確認。
![Xming](images/ss_xming.png){align=right, width=800}

タスクバーなどからubuntuアイコンを選択し、端末を開く。

!!! warning
    PowerShellなどで開いた端末だとX Window転送がうまく行かない！

2. repicdbへSSHログイン。個人のユーザでOK。

```
ssh -XY -l [your user] 192.168.100.104
```

!!! warning
    repicdaqXXではGUIがクラッシュします！必ずrepicdbで作業。


3. repicdb上で、以下のコマンドを実行し、解析用GUIを起動。

```
cd ~/
source /nas/qc/metrologyAnalysis/setup.sh
pmapp.py &
```

### 解析用GUI上の操作

プログラム（pmapp.py）を実行すると、以下のようなGUIが起動する。

- Component type
  - ITkPixV1xModule (Assembly後のモジュール)
  - ITkPixV1xModuleCellBack (Assembly後のOBモジュール、セル付き)
  - ITkPixV1xBareModule (bare moduleのreception test)
  - ITkPixV1xFlex (PCB reception/populatedの測定)
- Test stage
- Module name (Serial number)
  を入力して、
- サイズ測定のために取得したスキャン・ディレクトリの下にあるlog.txtファイルを選択する。

!!! note
  ファイルのパスは以下。
  /nas/PreProduction/Module/[ATLAS_SN]/[STAGE]/Module_[ATLAS_SN]_[STAGE]_PreProduction_Size_Front/log.txt

- Log fileはSizeとHeightの両方に設定する。
  - 現状では、Size測定として取得したスキャンのlog.txtを両方に設定する。（2023.08.01現在）
- モジュールの情報とLog fileを設定したら、"Process All"ボタンを押して処理を開始する。

![pmapp.py GUI](http://hpx.phys.ocha.ac.jp/user/tkohno/work/ITk/Metrology/Manual/pmma/pmma-gui2.png)

### 解析結果の確認

解析結果のまとめは中央の表に表示される(HeightとSizeのタブを確認)。設計値と許容範囲内の変数は緑、許容範囲外の変数は赤で表示される。色がついておらず黒字の変数は計算途中で使用したデータで、気にしなくてよい。

- Height測定
  - 通常、全て緑になるはず。
- Size測定
  - 自動のパターン認識では、現状赤がたくさん出る。その場合、手動でのパターン検出が必要である。

![高さ測定の表](http://hpx.phys.ocha.ac.jp/user/tkohno/work/ITk/Metrology/Manual/pmma/height-summary.png)
![高さ測定の表](http://hpx.phys.ocha.ac.jp/user/tkohno/work/ITk/Metrology/Manual/pmma/size-summary-red.png)

### 手動でのパターン検出

一番右のパネルに写真の一覧が表示される。それぞれの写真から自動検出された位置は赤い小さな点で表示されるが、それがずれている場合がある。そのときには、写真上の正しい位置を目視で確認し、ダブルクリックして点を打つことで自動検出の結果を上書きできる。画像の上でダブルクリックすると、青点が表示されこの座標が使用される。変なところに点を打ったら、もう一度ダブルクリックしてやり直す。点が更新されるたびに中央の表にも結果が反映される。

それぞれの写真の正しい位置については、このページ下部の[メトロロジー写真見本一覧](#メトロロジー写真見本一覧)を参照。

### 解析結果のファイルへの保存

GUIで指定したComponent typeに応じて、MODULE等のディレクトリが/nas/qc/metrologyDataの中に作成され、その下に"001"のような形式で、同じcomponentに対して複数回データを処理した際にその都度新しいディレクトリが作成される。この作業場所には、解析途中で作成した加工済み画像などが保存されている。

データの処理と手動での補正が完了したら、"Save (create JSON file)"ボタンを押すと、作業場所にdb.jsonとdata.pickleというファイルが作成される。db.jsonはデータベースに上げるためのデータである。（要検証）data.pickleの方は、アプリケーションの実行時の情報を丸ごと保存したもので、より詳細な計算結果等にもアクセス可能である。

!!! note
    GUI上では何も起こっていないように見えるけど。ファイルは出来ているはず。
    "/nas/qc/metrologyData/MODULE/[ATLAS_SN]/MODULE_ASSEMBLY/[Some Number]/db.json"
    が出来ているか確認するか、pmapp.pyを開いた端末にその旨のメッセージが出ているかを見ると良い。(改善されると親切)


## 3. 結果(メトロロジー解析と外観検査)のLocalDBへのアップロードの手順
### 解析のための環境設定とGUI起動
1. metrology machineの常駐プログラムリストなどで、Xmingが起動していることを確認。
2. タスクバーなどからubuntuアイコンを選択し、端末を開く。

!!! warning
    PowerShellなどで開いた端末だとX Window転送がうまく行かない！

3. 下記コマンドでrepicdaqXXへログイン(下はXX=1の例だがどのマシンでも良いはず)。
  ```
  ssh -l admin -XY 192.168.100.105
  ```

4. 下記コマンドを打ち、GUIを起動。
```
module-qc-nonelec-gui
```
### アップロードGUI上の操作
1. LocalDBへのログイン(user: itkqc)

2. MetrologyとVisual inspectionをGUIに従って進めていく。必要なファイルは以下。

- Metrology: /nas/qc/metrologyData/MODULE/[ATLAS_SN]/MODULE_ASSEMBLY/[Some Number]/db.json
- Visual Inspection (FRONT): /nas/PreProduction/Module/[ATLAS_SN]/[STAGE]/Module_[ATLAS_SN]_[STAGE]_PreProduction_VI_Front_vi.jpg
ここで、７、１２，２５，３０番目の写真にチップの番号が書いてあるので、スプレッドシートに書いてある番号とあっているかを確認する。
- Visual Inspection (BACK): /nas/PreProduction/Module/[ATLAS_SN]/[STAGE]/Module_[ATLAS_SN]_[STAGE]_PreProduction_VI_Back_vi.jpg

### メトロロジー写真見本一覧

![ASIC left](images/Metrology-AsicL.png){align=left, width=350}
それぞれ、ASICの左辺を選ぶ。（青点が正しい位置を示す。赤点は自動検出に失敗しているので無視。）

![ASIC right](images/Metrology-AsicR.png){align=left, width=350}
それぞれ、ASICの右辺を選ぶ。（青点が正しい位置を示す。赤点は自動検出に失敗しているので無視。）

![Sensor top](images/Metrology-SensorT.png){align=left, width=350}
それぞれ、センサー上辺を選ぶ。（赤点が正しい位置を示す。）

![Sensor bottom](images/Metrology-SensorB.png){align=left, width=350}
それぞれ、センサー下辺を選ぶ。（赤点が正しい位置を示す。）

![Sensor top](images/Metrology-FlexT.png){align=left, width=350}
それぞれ、Flex上辺を選ぶ。（上2枚は青点、下2枚は赤点が正しい位置を示す。）

![Sensor bottom](images/Metrology-FlexB.png){align=left, width=350}
それぞれ、Flex下辺を選ぶ。（赤点が正しい位置を示す。）

![Fiducial marks](images/Metrology-Fmark.png){align=left, width=175}
![Fiducial marks](images/Metrology-AsicFmark.png){align=left, width=175}
Flex側は円の中心、ASIC側は「F」字状のマークを選ぶ。（青点が正しい位置を示す。）
