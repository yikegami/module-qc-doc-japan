# 熱サイクル試験の手順
## サーマルサイクルの始め方

1. モジュールを吸着ヘッドに装着
2. NTC読みだしコネクタを使用するモジュールに接続
    
    コネクターは図のようにモジュールのソケットの下の段に接続し、向きは画像の通りに接続することに注意する
    

![NTC読み出しコネクタを接続](ThermalCyclingPics/Untitled.jpeg)

NTC読み出しコネクタを接続

![Untitled](ThermalCyclingPics/Untitled.png)

1. 使用するチャンネルの真空吸着をバルブを捻ってONにする
    
    バルブの向きが「－」の時真空吸着ON、「|」の時OFF 
    

![IMG_6367.JPG](ThermalCyclingPics/IMG_6367.jpg)

1. 恒温槽の扉をしっかりと閉め、ロックをかける
2. VNCサーバー192.168.100.6703に接続
3. ブラウザの127.0.0.1:8089/home/に接続
4. Thermal Cycleをクリック
5. フォームを満たしてSubmitをクリックするとTCスタート
6. Result画面が表示
7. Grafana上でTC StatusがONGOINGであることを確認

![Untitled](ThermalCyclingPics/Untitled%201.png)

![Untitled](ThermalCyclingPics/Untitled%202.png)

![Untitled](ThermalCyclingPics/Untitled%203.png)

### TC Form info

サーマルサイクルはRegular Cycle + Last Cycle で構成される

- **Machine :** マシン名を記入(REPICなど)
- **Number of Thermal Cycle :** 標準サイクルの回数を記入
- **Min Regular Temp :** Regular Cycleの最低温度を記入
- **Max Regular Temp :** Regular Cycleの最高温度を記入
- **Min Last Temp :** Last Cycleの最低温度を記入
- **Max Last Temp :** Last Cycleの最高温度を記入
- **Use Last Cycle :** チェックするとLast Cycleも実行される
- **Channel Select :** 使用するチャンネルのみチェック

### Initial Check

TCが始まるとGrafana上で確認可能

![TC開始前](ThermalCyclingPics/Untitled%204.png)

TC開始前

![TC中](ThermalCyclingPics/Untitled%205.png)

TC中

TC開始後、Initialの欄が全て✅になると温度変化が始まる

DP check : 露点が恒温槽の温度より10℃下回っているか

Vacuum check : 使用するチャンネルが吸着圧力が500mbar以上であるか

### Interlocks  
以下の状態になるとインターロックがかかりサイクルは中断され室温(25度)に戻る。

- Dewpoint : 恒温槽温度 > 露点温度 -2度
- Vacuum : 吸着の負圧 <>
### System Daemons
pythonファイルは"/home/admin/Titech_thermalcycle/tc_logger/TC_scripts/"下にある.
- tc_sht85.service  
    mon_sht85.py
- tc_arduino.service  
    mon_arduino.py
- tc_su262.service  
    mon_su262.py
- tc_interlock.service  
    interlock.py
- TC_app.service  
    /usr/bin/python3 manage.py runserver  192.168.100.7:8089
- ExecTC.service
    ExecThermalCycle.py
## Web app の立ち上げ方

1. ssh で192.168.100.7に接続

```bash
ssh admin@192.168.100.7
```

2. /home/admin/Titech_thermalcycle/tc_logger に移動

```bash
cd /home/admin/Titech_thermalcycle/tc_logger
```

3. start_app.shを実行

```bash
source start_app.sh
```

4. 127.0.0.1:8089/home/にサーマルサイクルのPC上で接続可能
