# シフトリーダーの役割

シフトリーダーは館山現地におけるモジュール生産とQCを原則として単独で運用し，シフトクルーを統率することのできるスタッフです．

## 基本姿勢

* 始業・休憩・終業時刻を守る
* 作業エリアの整理整頓
* 「ない化」「ても化」「にく化」を心がけ，作業環境の改善に務める

## 事前に行うこと

* シフターの連絡先を把握する(メールとスカイプ)。ITk Tateyama Lifeのチャットにシフトクルーを加える。
* シフターに、放射線バッチを持ってくるように注意喚起
* シフトの週に必要な部材が足りるかをあらかじめ前のシフトリーダーに確認する。
* シフトの前の週にハヤシレピックの担当者に作業伺いのメールを送る。(出張申請をこのメールをもとに行う場合があるので水曜日ごろがよい)
* 例、(先方のメールアドレスは事前に把握すること)
```
件名 : 8/21-25 の作業伺い

ハヤシレピック　田口様，磯川様，安西様，西田様，

いつも大変お世話になっております。高エ研の中村浩二です。

お盆明け21日より、御社館山工場での作業を再開させていただきたいと考えております。

参加者は、
高エ研 : 中村、中浜、生出
筑波大 : 学生1名
早稲田大 : 学生1名
の合計5名になります。

従来通り1日1台のペースでモジュールの接着をお願いできればと考えております。
ただし22日は見学対応をお願いすることとなっておりますので、朝ではなく、見学者の前での実演を午後にお願いするかと思います。詳細は21日以降に相談させてください。

どうぞよろしくお願いいたします。

中村 
```

## 在庫の確認

以下の在庫を確認する

* BareModule
* PCB
* キャリア
* Power/Data pigtails

在庫管理情報は申し送りで共有するとともに、[日本クラスタのGoogle Spreadsheetのinventoryタブ](https://docs.google.com/spreadsheets/d/1BrP1ZUhfLx81iy4winwhAItocJb6Eg7z8EUW_kOVk5g/edit#gid=1083699707)でも日付情報と数のアップデートを適宜行う。

!!! note Bare Moduleが正しい方向でケースに収められているかを確認(少なくともその週に作る分)。

## モジュールのシリアル番号の発行と管理

* PCBとBare Moduleを[ココ](https://docs.google.com/spreadsheets/d/1BrP1ZUhfLx81iy4winwhAItocJb6Eg7z8EUW_kOVk5g/edit#gid=660949188)と[ココ](https://docs.google.com/spreadsheets/d/1BrP1ZUhfLx81iy4winwhAItocJb6Eg7z8EUW_kOVk5g/edit#gid=1848418157)のシートから選定し、次の日の貼り付けに必要な個数分の組み合わせを決めて記入。
* LocalDBでモジュールのシリアル番号を生成する
* [Production Lineのシート](https://docs.google.com/spreadsheets/d/1BrP1ZUhfLx81iy4winwhAItocJb6Eg7z8EUW_kOVk5g/edit#gid=660949188)に新規モジュールを追加。
* ラベルメーカーTEPRA(SR-R7900P)を用いてキャリアにラベルを貼る
 (TEPRA LINK 2のソフトウェアにデフォルトで入っている"Supplies Managing"というテンプレートを流用して作成。)
* REPICワークシートを作成する ([PDFリンク](https://drive.google.com/file/d/190xIF0pq4mvhEcLxSVkSU2WjA7eXqZrN/view?usp=sharing))
* 注) ワークシートの"BareModule: □□-□"に書くべき番号はBare Moduleのケースの中に書いてある番号。本来はScratch IDになっているはずだが、HPKの人が間違えてSensorIDの番号を書いていることもあるので、必ずケースの中の現物を確認してからREPICの人へ引き渡すこと。[ここ](https://docs.google.com/spreadsheets/d/1BrP1ZUhfLx81iy4winwhAItocJb6Eg7z8EUW_kOVk5g/edit#gid=660949188)のsensor id(Col J)もしくはScratch ID(Col K,L)の数字のうち、いずれかが書いてあるはずなので必要であればチェックするとよい。Batch7のケースの一部はScratch IDではなくて、Sensor IDの番号が書かれていたため混乱が起こった。

## REPIC側とのやりとり（Assembly, Wirebonding）

基本的な流れは以下の通り． __この順序を飛ばさないことが重要．__

（[「モジュール・アセンブリ手順」](/assembly)のページも合わせて参照のこと．）

* 次の日アセンブリを行うBareModuleとPCBのペアを決めてトレーにPCBを準備し，ワークシートと共にREPICの方に引き渡す．
* 硬化の終わったアセンブリ済みモジュールのVisual InspectionとMetrologyをシフトクルーに行ってもらう．
* PCBの相対位置がBareModuleに対して許容範囲にあるか，目視とMetrologyで確認する．
* Visual InspectionとMetrologyの解析・登録を（リモート）シフトクルーに行ってもらう．
* 各FE ChipのIref Trimの値を確認し，ワイヤ・ボンディングを依頼する．
* ワイヤ・ボンディングの終了したモジュールのVisual Inspectionをシフトクルーに行ってもらう．
* Visual InspectionとMetrologyの解析・登録を（リモート）シフトクルーに行ってもらう．
* Wirebonding時の作業者，プルテストの情報を登録を（リモート）シフトクルーに行ってもらう．
* Wirebonding後，ただちにE-fuseの確認を行う．

## Readout Tests

* どのBoxでどのモジュールの試験が行われているかを忘れないように常に把握する
  ([Grafana Shift Leader Board](http://192.168.100.104:3000/d/qBnfsIr4z/shift-leader-board?orgId=1&refresh=5s)を活用する)
* CoolingBoxのInterlockとその対処法に精通する．
* テストの手順を正しく理解し，シフターが迷ったときに正しく導けるようにする
* それぞれの試験の結果をひと目見て，先のテストに進めるかどうかを判断する

## その他、シフトリーダの責任で行う作業

以下の作業は、シフトリーダ自身、もしくは監督のもとで信頼できるシフターのみが行うこと。

* モジュールのステージサインオフ、および恣意的なステージ変更
* ６枚モジュールを保管するプレートへのモジュール出し入れ。
* サーマルサイクルの一連の作業。
* サーマルサイクル後の写真撮影(キャリアから外す作業が入るため)。

## シフトのログと引き継ぎ

* Moduleの製造やテストの進捗状況をProduction Lineの[Googleシート](https://docs.google.com/spreadsheets/d/1fCHiRRp5fFAixouX3U1oIQ0DX0SNlsu-lbWTl--Q5zY/edit#gid=1292067329)に記入。
* 1日の作業ログを[e-log](http://atlaspc5.kek.jp/elog/ATLAS+pixel/)に投稿 (過去の"PreProduction Operation MM/DD"のエントリーを参考にするとよい)
* 一週間の作業のまとめをシフトの最終日終了後に[e-log](http://atlaspc5.kek.jp/elog/ATLAS+pixel/)に投稿 (過去の"PreProduction Operation (MM/DD-MM/DD)"を参考にするとよい)
* その週のシフター、次の週のシフター、次の次の週のシフトリーダ宛に引き継ぎメールを送る。