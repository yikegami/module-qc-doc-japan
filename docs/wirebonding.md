# ワイヤボンディング後のIref trimの確認とプルテストの登録

## Iref trimの確認

* REPICの方（多くの場合宇井さん）がワイヤボンディングが終了したモジュールを持ってくる．

* LocalDB上でステージがWIREBONDINGに移行していることを確認する．

* Metrologyマシンを用いてワイヤボンディングステージでの写真撮影とワイヤ高さ計測を行う．手順はアセンブリステージに準拠する．
  
!!! warning
	ワイヤのついている裸のモジュールの取り扱いは特に注意してください．使う指以外の指が当たったりしないように細心の注意をお願いします．

* `module-qc-nonelec-gui`を`repicdaq[1-3]`のいずれかのマシンから起動する．VNCが読み出し作業で占有されている場合は`ssh -XY`でX-windowを飛ばすやり方で起動してください．

* Irefトリムの値は作業票に記入してある．この値と実際のワイヤパターンを比較する．GUIの各タイルにリファレンスがあり，そこに読み方が書いてある（ワイヤあり＝0，ワイヤなし=1）．

* トリムを読むべきタイル番号は，Chip1: 1, Chip4: 18, Chip2: 19, Chip3: 36

* Referenceタブに，どのワイヤを読めばよいか，対応が書いてある．Referenceタブとの切り替えは，写真をクリックした状態で「s」キーを押すことで切り替えられる．

* その他の操作はアセンブリ時のVisual inspectionと同様．裏面の写真は撮らない．

## Wirebonding情報の登録

* Machine: "K&S Asterion"
* Operator : "Suzuki"
* それ以外のフィールド： "N/A"
* 温湿度：卓上温湿度計かGrafanaのEnvMonに表示された値を記録

![wirebonding_info](images/wirebonding_info_example.png)

## Wirebondプルテストの登録

`REPICによるワイヤボンディングが終了すると，`/nas/qc/wirebond_pull_test/ATLAS_CSV/`の下に，`WB_PT_20UPGM22601022_20230711.csv`という形式のCSVファイルが作られる．(
このファイルはREPICの方がNAS上の書き込み専用フォルダにアップロードしたExcelフォーマットをcronjobでCSVに自動変換している．)

`module-qc-nonelec-gui`でpull testを選択し，このCSVを選んで登録する．
このとき，オペレータとプルテストの情報入力が求められますが，

* Operator: Otaki
* Machine: DAGE 4000

を記入してください．
