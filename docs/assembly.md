# モジュール・アセンブリ手順

* まず，[REPICワークシート](docs/REPIC%20Worksheet.pdf)を印刷する．（このワークシートを用いてWIREBONDINGまでの手順が正しく行われていることや，状態確認を行います．）
* PCB番号，シフトリーダー，ATLASシリアル番号を記入してREPIC側に渡す．BareModuleのID（w11-2等）もわかるところに書いてください．
* BareModuleを裏返してスクラッチマークの位置を視認し，向きが正しいを確認する（下図参照）

![BareModule Orientation](images/bare_module_orientation.png)

![Jig Orientation](images/assembly_jig_orientation.png)

* ATLASシリアル番号をLocalDB上で生成し，ITkPD上に登録する．
* 各FEのIref Trimの値を確認する．

* アセンブリ後，質量値をLocalDBから入力する．
    * Mass: ワークシートの「モジュール質量」欄の値を記入
    * Scale accuracy: 1 (ワークシートにも記載あり)
    
* 同様に，Glueの情報をLocalDBから入力する．

    * Ratio of epoxy mixture: 0.5
    * Glue Batch Number：ワークシートから書き写す
    * Name: REPICの作業者の方を書き写す（英語で）
    * Glue type: Araldite 2011
    * Room Temperature, Humidity: 卓上温湿度計もしくはGrafanaのEnvMonページで表示されている値を記入する
