# 困ったときは

## Particle Counterのモニタが停止している

* `repicdb`のDHCP serverを確認する．
  `repicdb: $ systemctl status dhcpd`
* Windows Mini PCにparticle counterをLAN-to-LANで直結して，DeviceInstallerを下のタブから起動して，Searchをかける．デバイスのIP設定を確認して，適切に設定する．

## 時報やデシケータ開放時のアラームが鳴らない

* メトロロジ用のWindowsマシンで、タスクスケジューラを開く。
* 左側のタスクツリーの中から、"タスクスケジューラ(ローカル)"->"タスクスケジューラライブラリ"->"ITkPix"を選択
* ”環境モニタ”のタスクを終了し、改めて実行する。
* Power Shellのウィンドウが開くので、最小化しておく(閉じてしまうと動作しなくなる)。

**原因:**
 バックグラウンド動作するプログラムになっていないので、何かの拍子にPower Shellのウィンドウが閉じられると手動再起動が必要になる。

## Metrology GUI Q&A

Metrologyの測定で困ったらここをみる。随時更新


## digitalscanでE-fuseが正しくないことが判明したとき （エキスパート向け）

正しく登録し直すためには，DB expert workが必要になります．

!!! note
    深呼吸をして落ち着いて取り組みましょう．

まず，digial scanの結果として読み取られたe-fuseの値をそれぞれのFEごとにメモしてください．念のため，connectivity JSONでenableフラグをFE一つずつenableして，確実を期すことを勧めます．

!!! warning
 e-fuseの照合が失敗するとき，YARRではいわゆる"new"と"old"という２種類のシリアル番号をデコードするアルゴリズムを試行します．この２種類のアルゴリズムは同一の結果を返すとは限らないので，e-fuseの読み出しの結果だけから一意にシリアル番号が決定できない可能性があります．

e-fuseの16進数シリアル番号候補からATLASシリアル番号`20UPGFCxxxxxxx`に変換し，そのシリアル番号の所在を確認します．場合によっては，そのシリアル番号はすでに他のBareModuleに使われている可能性もありますし，あるいはシリアル番号が存在しない可能性もあります．

* シリアル番号が存在しない場合は，正しいシリアル番号ではないので，new/oldのうちもう一つのシリアル番号が真のシリアル番号だと確定します．
* シリアル番号がすでにほかのBareModuleに使われていて，かつそのモジュールのQCが終わっている場合には，正しいシリアル番号ではないので，new/oldのうちもう一つのシリアル番号が真のシリアル番号だと確定します．逆に，モジュールのアセンブリ・QCが終わっていない場合にはそのシリアル番号は候補として残ります．

これらのチェックを経て，なおシリアル番号が確定しない場合には，シリアル番号を確定させる他の必要十分な情報が得られるまでそのモジュールについての作業は保留してください．

以下ではシリアル番号がすべて確定したと仮定します．

このとき，そのFEが他の未アセンブル・モジュールに属している場合には，そのFEをdisassembleします．FEがBareModuleに属してなく，Waferに属している，あるいは単独のコンポーネントとして存在する場合には，FEをassembleします．

同時に，Spreadsheetの該当するモジュールについて，FE chipの情報を正しく修正します．

!!! warning
    このとき，Iref trim, VDDD trim, VDDA trimの欄の再構成も忘れないようにしてください．

!!! note
    この作業を行った時点で，すでにモジュールまたはBareModuleをLocalDBの管理下に置いている場合には，LocalDBのモジュール─FEおよびBareModule─FEの対応関係が誤って記録されているので，これを更新する必要があります．この修正を行うためには，LocalDBで「Pull components from ITkPD」を行うことで，対応関係を再構築できます．


## DAQボード（Trenz Card）の不調

詳細な原因は不明ですが，長期間CoolingBoxを稼働させていると，DAQボード（Trenz Card）が不調をきたすことがあります．この不調は必ずしもわかりやすく判定できませんが，典型的には

* YARR Scanを行う際にMask Loop0の状態でログが進まなくなる（リトライしてもダメ）．VNCなどリモート接続やネットワークには（この時点では）特に問題が見られない．
* YARR Scanあるいは`specComTest`などのTrenz Cardにアクセスするプロセスの実行途中でカーネルパニックが発生し，それがREPIC LAN全体に悪影響をおよぼし，どのPCにも接続できなくなってしまう．

といった挙動パターンが知られています．

特にネットワークの不調がひとたび発生すると，できることが非常に限られるため，以下の処置を行い，迅速に対応してほしい．

### VNCなどのDAQ PCへのリモートアクセスができる場合

1. 実行途中のYARR Scanはkillして，`RunElecTests.sh`もquitする．

2. `pidClient`から`setHV_Off`，`setLV_Off`を行い，`resetV 0`を実施した後に`kill_server`を行って`pidServer`をシャットダウンする．

3. Texio PFR（ペルチェ電源）の主電源を落とす

4. チラーのポンプ，温度制御を止めた後に背面の黒い主電源スイッチを落とす

5. CoolingBoxの緊急停止ボタンを押す

6. PCの再起動を行う

7. PCが起動したらArduino MEGAとの通信ができているかを確認する．
```
cd ~/titech_cool/pid
python3 lib/ArduinoSingleBoxController.py`
```
を行ってArduinoからの読み出し結果が返ってくればOK．

8. `/nas/daq/ITkpix-v1/YARR-1.28Gbps/bin/specComTest`を実施し，まともな応答が返ってくるかどうか確認する．以下のようなログであればOK．
```
void SpecCom::init() -> Opening SPEC with id #0
void SpecCom::init() -> Mapping BARs
void SpecCom::init() -> Mapped BAR0 at 0x0x7f075e4b2000 with size 0x100000
void SpecCom::init() -> Mmap failed
void SpecCom::init() -> Could not map BAR4, this might be OK!
Starting DMA write/read test ...
... writing 8192 byte.g
... read 8192 byte.
Success! No errors.
```

8. CoolingBoxの緊急停止ボタンを右に90度ひねって解除する

9. チラーの背面の黒い主電源スイッチを上げ，アラートが出ないことを確認する．

10. チラーのポンプを再稼働する

11. チラーの温度制御（Run/Stop）を再稼働する

12. Texio PFR（ペルチェ電源）を立ち上げて30秒待つ．
```
cd ~/titech_cool/pid
./reset.sh
```
を実施し，Arduinoのステートが`Normal`であること，ペルチェ電源のVIスキャンが(-10V, 10V)のレンジで概ね線形であること（|V|<3Vを除く）を確認する．

13. `pidServer.py`を立ち上げる．


### DAQ PCへのリモートアクセスができない場合：まず安全確保

!!! warning
	CoolingBoxの制御が妥当に行われている保証がありません．Grafanaへの表示はClosed network越しに記録されるので，影響を受けにくくしてありますが，カーネルパニックが発生した場合はプロセスが正常動作し続ける保証がありません．安全確保をするための迅速な対応が求められます．
    
!!! info
	これが起きてしまうと，`ssh`リモートログインもまともに使えない可能性が高いです．

1. どの`repicdaq[1-3]`が問題を生じたかを特定する．これを確認するためには，REPIC LANに繋がっている側のEthernetケーブル（手前側）を１つずつ抜き，ネットワークが回復するかどうかを`ping 192.168.100.104`で確認する．ネットワークが回復した場合，Ethernetケーブルを抜いたPCが容疑者となる．

2. ネットワークが復旧したら，問題のPCでコントロールしているモジュールののHVとLVを遮断し，安全をはかる．これを行うためには，

* repicdaq1の場合: 
```
snmpset -OqvUp +14.12 -v 2c -m +WIENER-CRATE-MIB -c guru 10.0.0.2 outputSwitch.u300 i 0
snmpset -OqvUp +14.12 -v 2c -m +WIENER-CRATE-MIB -c guru 10.0.0.2 outputSwitch.u0 i 0
```
* repicdaq2の場合: 
```
snmpset -OqvUp +14.12 -v 2c -m +WIENER-CRATE-MIB -c guru 10.0.0.2 outputSwitch.u301 i 0
snmpset -OqvUp +14.12 -v 2c -m +WIENER-CRATE-MIB -c guru 10.0.0.2 outputSwitch.u1 i 0
```
* repicdaq3の場合: 
```
snmpset -OqvUp +14.12 -v 2c -m +WIENER-CRATE-MIB -c guru 10.0.0.2 outputSwitch.u302 i 0
snmpset -OqvUp +14.12 -v 2c -m +WIENER-CRATE-MIB -c guru 10.0.0.2 outputSwitch.u2 i 0
```

を極力速やかに行う．

3. Texio PFR（ペルチェ電源）の主電源を落とす

4. チラーのポンプ，温度制御を止めた後に背面の黒い主電源スイッチを落とす

5. CoolingBoxの緊急停止ボタンを押す

6. PCの再起動を行う

7. PCが起動したらArduino MEGAとの通信ができているかを確認する．
```
cd ~/titech_cool/pid
python3 lib/ArduinoSingleBoxController.py`
```
を行ってArduinoからの読み出し結果が返ってくればOK．

8. `/nas/daq/ITkpix-v1/YARR-1.28Gbps/bin/specComTest`を実施し，まともな応答が返ってくるかどうか確認する．以下のようなログであればOK．
```
void SpecCom::init() -> Opening SPEC with id #0
void SpecCom::init() -> Mapping BARs
void SpecCom::init() -> Mapped BAR0 at 0x0x7f075e4b2000 with size 0x100000
void SpecCom::init() -> Mmap failed
void SpecCom::init() -> Could not map BAR4, this might be OK!
Starting DMA write/read test ...
... writing 8192 byte.g
... read 8192 byte.
Success! No errors.
```

8. CoolingBoxの緊急停止ボタンを右に90度ひねって解除する

9. チラーの背面の黒い主電源スイッチを上げ，アラートが出ないことを確認する．

10. チラーのポンプを再稼働する

11. チラーの温度制御（Run/Stop）を再稼働する

12. Texio PFR（ペルチェ電源）を立ち上げて30秒待つ．
```
cd ~/titech_cool/pid
./reset.sh
```
を実施し，Arduinoのステートが`Normal`であること，ペルチェ電源のVIスキャンが(-10V, 10V)のレンジで概ね線形であること（|V|<3Vを除く）を確認する．

13. `pidServer.py`を立ち上げる．

### inital warmの最初の試験において、local DBにConfig Revisionsが生成されない

ITkPDへのアクセスのトークンが期限切れが疑われる。top pageからInitial data sync with ITkPDをすると認証を求められ、それでトークンを更新することができる。更新後に該当モジュールのページにアクセスすることでConfigはLocalDBに生成される。

