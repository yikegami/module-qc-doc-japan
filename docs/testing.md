# モジュール読み出しQC手順

## 基本操作

* VNC clientを用いたリモートデスクトップ環境越しにタスクを行います．
* ターミナルはデスクトップの任意の場所でマウスを右クリックして，Open Terminalを選ぶことで開けます．
* 最低，２個のターミナルを開きましょう．(1) `cd ${EQC_OPERATOR_DIR}` (2) `cd ~/titech_cool/pid` にそれぞれcurrent directoryをセットしておくのが望ましいです．(1)はDAQ制御用(`RunElecTests.sh`)，(2) はDCS制御用 (`pidClient.py`)を開いておきます．

![VNC](images/vnc_example.png)

## モジュールの交換

!!! warning
    モジュールの交換時に，CoolingBoxのコールドヘッド部が外気に晒されます．コールドヘッドの直下には-15℃前後の冷媒が流れていて，コールドヘッドを常温に保つためにペルチェ素子を用いて温めています．この状態はペルチェがもし機能しなくなれば即座に結露が始まる不安定な状態であることを認識し，コールドヘッドを外気に露出させた状態で長時間放置しないようにしてください．

!!! note
    もし，モジュールを投入しない状態で放置する場合は，窒素ガスのチューブをデルリンのケースの中に入れた状態で，断熱カバーを上からかぶせ，「ロックを閉めない」半開き状態で保全してください．内部の露点を極力下げて断熱，外気との接触を減らすことで結露しにくい状態を維持できます．

1. `pidClient`で`setHV_Off`, `setLV_Off`の順で実施し，`setT exchange`とすることで交換に適し温度（30℃）に設定します．
2. 温度が露点よりも10K程度高い状態（概ね25℃前後）になったら，`unlock`コマンドで断熱カバーを解錠します．このとき，LVやHVが入っていたり，外気露点との内部温度の相対的な関係で条件が満たされていない場合は解錠できませんので，条件が満たされていることを確認してから再度行ってください．
3. ピッグテールを取り外します
4. Power pigtailの下にある窒素ガスのチューブを抜き取ります．復元力で誤ってモジュールに触れないように，チューブをPower boardと断熱壁側面の間に差し込むなどしてください．
5. SHT85温湿度計のついたキャリアカバーを取り外します
6. モジュールをコールドヘッドから取り外します．

以上で取り外しは終了です．新しいモジュールの取り付けはこの逆の作業を行います．

1. モジュールをコールドヘッドにマウントします．
2. SHT85温湿度計のついたキャリアカバーを取り付けます．
3. Power pigtailの下にある窒素ガスのチューブを取り付けます．
4. ピッグテールを取り付けます．
5. Power pigtailを取り付けたら，Grafana上でModule NTCの読み出しが無事に再開していることを必ず確認してください．また，キャリア内部の露点が急激に下がっていくことを確認してください．
6. 断熱カバーを閉じて前後のロックを施錠してください．Grafana左側のCover Statusのインジケータが「Closed」になっていることを確認してください．
7. `pidClient`で`setT warm`など所定の温度に目標値を設定し，`setLV_On`などオペレーションに入ります．

!!! warning
    NTCの読み出しが正しくない状態で断熱カバーを閉じると，CoolingBoxのインターロックが発動します．その場合の復帰はエキスパートによる迅速な対処が必要（面倒）なので，蓋を閉じる前にNTCの読み出しができていることは必ず確認してください．

!!! warning
    モジュールの交換時には，Power pigtailを接続するときに「ずれ」を生じないように特に注意してください．結線後，NTCの温度の読みが25℃付近を指していることを __必ず確認__ します．

!!! warning
    もし温度の読みが正常でない場合はCoolingBoxの蓋を決して閉じないこと．

!!! warning
    Data pigtail側の接続を丹念にチェックします．特に「モジュール側」のZIFコネクタで接続不良がある場合にはデバッグに時間がかかるため，念入りにチェックします．

!!! warning
    SHT85の露点の読みが-5℃以下になっていることを確認してください．

Power pigtail, Data pigtail, 乾燥窒素ラインのすべての接続が良好の場合，はじめてCoolingBoxの蓋を閉じます．

### CoolingBoxの基本操作方法(LV, HV)

基本、モジュールの読み出しを行う際はLV，HVをONにして行います(`zero bias scan`は例外で，HV OFFのままやる)．
LV/HV操作は`pidClient.py`を実行することでできますが、一部のON/OFFは`RunElecTests.sh`が行ってくれます。

```
#PATHは通っているのでどのディレクトリでも実行可能

[admin@repicdaq1 ~]$ pidClient.py 
Press Ctrl-D to close the prompt.
 Type help to glance guides.
CoolingBox >>> help

Documented commands (type help <topic>):
========================================
EOF          relaunch_server   setHV_On      setMaxD         setTimeOffsetI
exit         resetDCS          setIduration  setMaxI         setTimeOffsetP
help         resetMPOD         setKd         setMaxP         setVerbosity  
kill_server  resetV            setKi         setRelayOff     stopPID       
log          restartPID        setKp         setRelayOn      unlock        
params       restoreInterlock  setLV_Off     setT          
quit         setHV_Off         setLV_On      setTimeOffsetD

LV ON command ::: 
CoolingBox >>> setLV_On

LV OFF command ::: 
CoolingBox >>> setLV_Off

HV OFF command :::  
CoolingBox >>> setHV_Off
```

!!! info
    `setHV_On`というコマンドを実装していましたが，2012/12/12に廃止しました．
    これは，HVを上げるタイミングは，QCフローの中で特定のステップで行い，その判断は`RunElecTests.sh`の中で自動判定するようにしたためです．

### CoolingBoxの設定温度の変更

```
$ pidClient.py 
Press Ctrl-D to close the prompt.
 Type help to glance guides.
CoolingBox >>> help setT
setT <value>: set the target temperature to the specified value. Special enum are appointed
  - exchange     : +30 degC
  - warm         : +20 degC
  - cold         : -15 degC
  - cold_startup : -35 degC

CoolingBox >>> setT warm

or

CoolingBox >>> setT 30
```
!!! info
    `THERMAL_CYCLES`はwarm環境で試験してください。

## 作業チートシート

[作業チートシート](docs/qc_flow_20231013.pdf)(INITIAL_COLDまで)はラミネート印刷して配備してあります．
一人一枚デスクの横に置いて活用してください．

## モジュールのステージについて

2023/12現在、館山では以下のステージの読み出しQCが行われています。ステージごとに手順がわずかに異なりますので、自分が取り組んでいるのがどのステージなのかよく把握しつつ、以下の説明を読んでください。なお、ステージとそこで行われるべき試験については、このページの[各ステージで実施するテスト一覧](#各ステージで実施するテスト一覧-20231206現在thermal_cyclesまで)に概要が、またlocal DBの各モジュール・チップの最下部にCheck QC Flowという欄に読み出し試験以外の項目も記載されていますので、合わせて確認してください。

* `INITIAL_WARM`
* `INITIAL_COLD`
* `PARYNELE_UNMASKING`
* `POST_PARYLENE_WARM`
* `POST_PARYLENE_COLD`
* `THERMAL_CYCLES`
* `LONG_TERM_STABILITY_TEST`
* `FINAL_WARM`
* `FINAL_COLD`

### PARYLENE_UNMASKINGステージでの特殊な取り扱いについて

!!! warning 2023/12現在 'PARYLENE_UNMASKING'の場合、MHT,PFAテストする前に以下のコマンドの実行が必要
	```
    $ cd /nas/qc/jp-itkpd-scripts/localdb
    $ python3 add_fe_missing_tests_parylene_unmasking.py <module_SN>
    ```

## FE config fileのダウンロード

`INITIAL_WARM`ステージからQCを始める場合，ITkPDからFE config filesをダウンロードを行います．

!!! info
    FE config fileとはモジュールの中の４枚のASIC（FE Chip）を動かすためのパラメータ設定ファイルのことです．<br />
    <ul><li>チップ全体に関わる200あまりのグローバル・パラメータ</li><li>ピクセルごとに関するパラメータ</li><li>キャリブレーションのためのパラメータ</li></ul>に分かれています．<br />各ピクセルごとのパラメータは，<ul><li>Enable Mask (EnMask),</li><li>Threshold DAC (TDAC),</li><li>HitBus,</li><li>Injection Enable (InjEn)</li></ul>があります．後者２つに関しては興味がありませんが，前者２つはとても重要です．<br />
    Electrical QCではキャリブレーションのためのパラメータを得るための調整（ADC Calibration等）を行うほか，グローバル・パラメータや各ピクセルごとのパラメータの調整を行います．

!!! warning
    2023/12現在、`INITIAL_WARM`ステージ以外の初期config作成は次のセクション[Local Sign-offを行ったときのLocalDBでのFE configの作成](#local-sign-offを行ったときのlocaldbでのfe-configの作成)を参照して行うこと。

Config fileは`/nas/daq/QCtest/module`ディレクトリー上の`moudleSN`に保存されるので要確認  

```
$./ConfigDownload.sh 
======== Defolt Env Setting =============
moduleSN     : 20UPGM22601029 <- moduleSNが正しいか確認!
Config Type  : L2_warm
INSTITUITON  : HR <- HR :ハヤシレピック
LocalDB_HOST : 192.168.100.104 
LocalDB_PORT : 5000
HV Setting   : -80 [V]
=========================================

Input accessCode1: #ATLAS PDBのaccessCode1を入力
Input accessCode2: #ATLAS PDBのaccessCode2を入力
```

ダウンロードが完了したら`/nas/daq/QCtest/module`ディレクトリー上に`moduleSN`に保存されいるか確認

!!! info
    `ConfigDownload.sh`で展開されるディレクトリには，FE chip configファイルの他に，connectivity fileと呼ばれるファイルがあります．<br />
    また，connectivity fileやFE config fileは`warm`, `cold`, `LP`の３種類があり，使い分けます．
    <ul><li>`warm`は+20℃で試験する場合，</li><li>`cold`は-15℃で試験する場合，</li><li>`LP`は+20℃，-15℃共通で，low power mode (LPM)で試験する場合</li></ul>にそれぞれ用いられます．<br />connectivity fileの中ではReadout Cardのなかでどの配線がどのFE chipと接続しているか，またどのFE chipをenableしているかを指定しています．

!!! info
    過去に一度`ConfigDownload.sh`が走っている場合には`FE config`を上書きしてしまいます．このため，2023/12/12以降，ファイルが存在する場合には上書きするか尋ねてきます．通常は，この上書きを行って再度ダウンロードする必要は，ありません．

## Local Sign-offを行ったときのLocalDBでのFE configの作成

!!! warning
    2023/12現在、こちらはINITIAL_WARMステージ以外で実行する必要がある。INITIAL_WARMステージでは上の[config fileのダウンロード](#config-fileのダウンロード)を行うこと。

Local Sign-offを行って次のステージに行った場合は，前のステージのconfigを引き継いで，LocalDBにFE configのスタートポイントを作成する必要がある．

例："INITIAL_WARM"を引き継いで，"INITIAL_COLD"にコピーする場合には，（`repicdaq[1-3]`で）以下の通り実行します：

```
cd /nas/qc/jp-itkpd-scripts/localdb
python3 inherit_config_from_prev_stage.py 20UPGM22601042 "MODULE/INITIAL_WARM"  "MODULE/INITIAL_COLD"
```

各Front End ChipのConfig Revisionsで適切なStage名の下にMode: warm, cold, LPのエントリがそれぞれあれば適切に作成できている。

!!!warning
    上の操作を行った際に，LocalDB上でFE Chipのページの下段にある”Config Revisions"パネルで，該当ステージのconfigが存在することを確認してください．

!!!info
    "PARYLENE_UNMASKING"以降のステージについても，同様にできます．原則として，1つ前のelectrical QCを行ったステージからconfigを取得してください．

    ```
    cd /nas/qc/jp-itkpd-scripts/localdb
    python3 inherit_config_from_prev_stage.py 20UPGM22601042 "MODULE/INITIAL_COLD"  "MODULE/PARYLENE_UNMASKING"
    ```
    
    ```
    cd /nas/qc/jp-itkpd-scripts/localdb
    python3 inherit_config_from_prev_stage.py 20UPGM22601042 "MODULE/PARYLENE_UNMASKING"  "MODULE/POST_PARYLENE_WARM"
    
    ```
    ```
    cd /nas/qc/jp-itkpd-scripts/localdb
    python3 inherit_config_from_prev_stage.py 20UPGM22601042 "MODULE/POST_PARYLENE_WARM"  "MODULE/POST_PARYLENE_COLD"
    ```
    ```
    cd /nas/qc/jp-itkpd-scripts/localdb
    python3 inherit_config_from_prev_stage.py 20UPGM22601042 "MODULE/POST_PARYLENE_COLD" "MODULE/THERMAL_CYCLES"
    ```

## Electrical QCの制御（`RunElecTetsts.sh`)

!!! warning
    `INITIAL_COLD`及び`POST_PARYLENE_COLD`ステージでは、-35℃で行う必要がある検査がいくつかあります。LV offの状態で温度を-35℃に下げ、[First Power Up + Digital Scan](#first-power-up--digital-scan)を参考に必要な試験を行ってください。その後-15℃に設定し、この項目で指定される試験を行ってください。

!!! info
    `RunElecTests.sh`は、以前使用されていた`ScanOperator.sh`, `SimpleScanOperator.sh`, `IVScan.sh`の試験用のスクリプトを統合したスクリプトです。

解析ディレクトリ

```
$cd /nas/daq/QCtest/module-qc-operator
# または　cd ${EQC_OPERATOR_DIR}
```

にて、

```
$./RunElecTests.sh -h
```

とすると基本的な使用法が表示されます。モジュールがLV ONの状態で各ステージで指定された温度に安定したことが確認出来たら、

```
$./RunElecTests.sh
```

を実行すれば対話形式で試験が進行します。LVやHVのコントロールもこのスクリプトが確認します。使用法でも表示されるように、試験は

```
  *  scan.20.1111            # POR Scan
  *  scan.-1.1111            # Chip configuration
  *  scan.21.1111            # EyeDiagram-based CmlBias Scan
  *  scan.22.1111            # CmlBias1 scan
  *  scan.15.1111            # Zero-bias Threshold Scan

  *  normal.iv-measure.1111        # IV Scan

  *  normal.adc-calibration        # 4 FEs together

  *  normal.analog-readback.1        # FE1 only
  *  normal.analog-readback.2        # FE2 only
  *  normal.analog-readback.3        # FE3 only
  *  normal.analog-readback.4        # FE4 only

  *  normal.sldo.1            # FE1 only
  *  normal.sldo.2            # FE2 only
  *  normal.sldo.3            # FE3 only
  *  normal.sldo.4            # FE4 only

  *  normal.vcal-calibration        # 4 FEs together
  *  normal.injection-capacitance       # 4 FEs together

  *  lp.lp-mode                # 4 FEs together
  *  lp.overvoltage-protection        # 4 FEs together
  *  lp.undershunt-protection        # 4 FEs together

  *  normal.data-transmission        # 4 FEs together

  *  scan.mht                # YARR minimal health test scans
  *  scan.tun                # YARR tuning Scans
  *  scan.pfa                # YARR pixel failure analysis scans
  *  scan.14                      # YARR X-ray Scan
```

の順で行われます。ステージによっては全ての項目が必要ではないため、[各ステージで実施するテスト一覧](#各ステージで実施するテスト一覧-20231206現在thermal_cyclesまで)を参考にしつつ進めると良いでしょう。

!!! info
    2023/12/08以降，Full Electrical Testを要求するステージと，簡易版Testを行うステージではPrimListが自動的に振り分けられるようにしました．

それぞれの解析が完了するとlocaldbの各FEチップのページに結果がアップロードされているのでそれを確認します。

!!! info
    2023/12現在local DBでQC passとされた場合は解析結果を基にconfigが自動更新されますが、QC failとなった場合は人間の判断が必要となります。<br />
    基本的には，「測定が正しく行われたこと」を次に進む基準とします．

基準は[こちら](https://docs.google.com/presentation/d/12n8hPfCJX65FsLQ4PxU83Ms1EkoQdPGfL79APu7kudE/edit#slide=id.gc6f73a04f_0_0)を確認。
問題と思われる場合は`b`で戻って同じスキャンを取り直すか、EXITして詳細な調査・調整をしましょう。

!!! note 
    HVは`RunElecTests.sh`が管理してくれます．<br />ユーザが操作する必要があるのは，モジュール交換時などにHVを最終的にOFFにする場合です．

!!! warning
    LVをOnにした後、GrafanaでLVの電流値/電圧、温度が適切であるか確認すべし  
    電流値:5.88 A, 電圧値2.2V前後 (2.0付近の場合、configが通らないことがあるので注意)  

!!! info
    EyeDiagram-based CmlBias Scanは画像を閉じると先に進みます。
    
!!! warning
	2023/12現在，Chip-by-chipで試験を行うAnalog ReadbackおよびSLDOの試験は，`eyeDiagram`を調整したときの「１回目」の調整が失敗する不具合があります．これはLBNLの提供するプログラム自身の問題で，われわれではすぐ直せません．ただ，このエラーが出る場合に単に試験をリピートすれば「２回目」にはうまく試験が実施できる場合がほとんどです．Analog ReadbackとSLDOではコア・スクリプトのexit codeが異なるためAnalog Readbackでは「失敗」を検知できるので「リピート」を選択して再実施します．SLDOの場合は「失敗」を検知できないので次のFEの試験に移ってしまいますが，「戻る」を選択すれば１ステップ戻って再実施できます．

!!! warning
    2023/12現在Low Power Mode, Overvoltage Protection, Undershunt Protectionは実行できず、実行しようとしてもスキップされます。これは問題ありませんので先に進んでください。

!!! info
    IV ScanではGUIが表示されデータの入力が求められます、[IV scan registration](#iv-scan-registration)を参考にtxtファイルを登録してください。

何らかの理由で途中でEXITする必要があった場合は、再度最初から初めて既に終わった試験をsでスキップして、再開するところまで進めてください。

### First Power Up + Digital Scan (-35℃でのpower up test）

低温での試験では、-35℃で最初に電源が入れられるかを確認します。-35℃に温度が下がったことを確認したら、LVをOnにし、

```
./RunElecTests.sh -t scan.20
```

を実行してください。

Power-upのしきい値の値が3000 V/s以下であることが確認出来たら、

```
./RunElecTests.sh -t scan.-1
```

でchipの初期コンフィグができるかを確認してください。出力されるlogで4 laneそれぞれの通信でsynchronizedと表示されれば問題ないです。

以上で-35℃での試験は終了です。-15℃に設定し、[Usage of RunElecTetsts.sh](#usage-of-runelectetstssh)に従い通常の試験を行ってください。

### IV scan registration

![moudle-qc-nonelectrical-gui (IVcure)](images/IV_curve_at_QCtesting.jpg) 
上記ポップアップが表示されたら、以下の手順に従って進める。

```
>>GUI上でuser : itkqc, PW : REPIC daqと同じを入力
>> moduleSNを入力
# ここでIVscanモードになっていることを確認 -> IVscanの選択肢が無い場合はmoduleのステージが適切なものになっているかlocaldbで確認
>>localdbにアップロードする IV scan結果(json file)を選択
# /nas/qc/ivcurve/repicdaq1/moduleSNディレクトリー上のjson fileを選択
>> アップロードを選択して完了
```

!!! info
    2023/12/08以降，最後に行ったIV scanの結果には，`/nas/qc/ivcurve/repicdaq1/moduleSN/last_result.json`というシンボリックリンクが張られました．通常これを選択すれば良いです．

アップロードが完了するとlocalDB上でIVscan結果を確認。赤い線がBreak Downと解析された線。（表示されないこともある。）110 V以下でBreak Downしている場合は現場で議論。  

### PrimListで特定のFE Chipだけdisabledにする

例えば，FE2だけをdisabledにする場合，

```
$ ./RunElecTests.sh -i 1011
```

とします．

### テストが終了したら

local DBで各試験を登録する必要があります。  

0. local DBにsign inしていない場合はsign inする。  
1. 試験したmoduleを構成するFront End Chipのページに行き、ページ下部で赤背景で表示されているMinimal Health TestのCheckout Scansをクリックする  
2. 右側のtagとdateを参考に該当するrunを選択する。 特殊な試験をしなければ、MHTのタグが付いた最新のものを選ぶなど。全てのscanで一つずつ選ぶ。  
3. Proceedの上のチェックマークがつけていれば、1 chipでの登録がすべてのchipで反映される。問題なければ一番下のProceedを選択。 登録後バックグラウンドで解析が行われ、解析が終わるとその項目が緑になる。（待たずに次のcheck outを進めて良い。）  
4. 残りの赤背景の項目に関しても上記手順を繰り返す。  
5. 全てのFront End Chipで登録した3項目が緑になっていることを確認する。  
6. 試験したmoduleのページに戻り、Electrical Test Module SummaryのCreate Summaryをクリックする。  
7. 行った試験がすべて表示されるので、行ったQC項目それぞれで最終結果とするテスト結果を選択。現在はfaildでも問題ない。登録結果のないテスト項目はそれでよいのかよく確認。  
8. 一番下のProceedを選択。シフトリーダーにLocal sign offを依頼する。この時チップの癖（マスクをした、応答の悪いチップがあったなど）があれば口頭で報告してシフトリーダーがlocal DBにログを残すか、シフター自身でlocal DBに登録する。  

!!! warning
    2023/12現在Low Power Mode, Overvoltage Protection, Undershunt Protectionは実行できないため、赤背景のままで先に進んでください。

### Coldテストが終了したら

マスクなしで完遂できればQC Sucsess、一部をマスクしてスキャンを完遂できたならConditional Pass、完遂できなければQC Failed。詳しい基準や個別事象の判断はシフトリーダーと相談のこと。

## 各ステージで実施するテスト一覧 (2023/12/06現在，THERMAL_CYCLESまで)

!!! note
    基本的にCML Bias scanは実行することを推奨します。

!!! note
    Analog readback及びSLDOはデフォルトでは1チップ毎4回走ります

!!! note
    慣習的にADC calibrationからData transmisionをSimple Scanと呼ぶことがあります。またPower-upをPORと呼ぶこともあります。

!!!warning
    誤って予定されていないスキャンを走らせた場合、現在のlocal DBの仕様ではQC passだと自動的にconfigがアップデートされてしまいます。その後の比較に影響が出る可能性があるため、シフトリーダーに報告してください。

|                          | INITIAL_WARM | INITIAL_COLD | PARYNELE_UNMASKING | POST_PARYLENE_WARM | POST_PARYLENE_COLD | THERMAL_CYCLES |
| ------------------------ | ------------ | ------------ | ------------------ | ------------------ | ------------------ | -------------- |
| powe-up @ -35℃           |              | x            |                    |                    | x                  |                |
| powe-up                  | x            | x            |                    | x                  | x                  |                |
| chip configration        | x            | x            | x                  | x                  | x                  | x              |
| CML Bias scan (2 types)  | x            | (x)          | (x)                | (x)                | (x)                | (x)            |
| zero bias threshold scan | x            |              | x                  |                    |                    |                |
| IV scan                  | x            | x            | x                  | x                  | x                  | x              |
| ADC calibration          | x            | x            |                    | x                  | x                  |                |
| Analog readback          | x            | x            |                    | x                  | x                  |                |
| SLDO                     | x            | x            |                    | x                  | x                  |                |
| Vcal-calibration         | x            | x            |                    | x                  | x                  |                |
| injection-capacitance    | x            | x            |                    | x                  | x                  |                |
| LP mode                  | x            | x            |                    | x                  | x                  |                |
| Overvoltage protection   | x            | x            |                    | x                  | x                  |                |
| Undershunt protection    | x            | x            |                    | x                  | x                  |                |
| Data transmission        | x            | x            |                    | x                  | x                  |                |
| Minimum Health Check     | x            | x            | x                  | x                  | x                  | x              |
| Tuning                   | x            | x            |                    | x                  | x                  |                |
| Pixel Failure Analysis   | x            | x            | x                  | x                  | x                  | x              |
| X-ray scan               | x            | x            | (x)                | x                  | x                  | x              |

!!!warning
    2023/12現在LP Mode, Overvoltage Protection, Undershunt Protectionは実行できず、実行しようとしてもスキップされます。これは問題ありませんので先に進んでください。
!!!note
    2024/2現在PARYLENE_UNMASKINGでのX-ray scanは原則取得推奨ですが、時間との相談でスキップも可、となっています。


## 詳細情報（デバッグ）

### 環境変数の登録 (setup.sh)

!!! info
    モジュールを交換した場合にはこの操作は必須ですが，2023/12/12以降，`pidClient.py`と連携し，CoolingBoxの蓋を`unlock`したときに，モジュールのシリアル番号を入力するミニ・ウィジェットが自動的にポップアップする仕様にしました．このウィジェットは内部で`setup.sh -c config/setup/repicdaq1.json`を呼び出しています．このため，ほとんどの場合でオペレータが`setup.sh`を直接に呼び出す必要性はなくなりました．

!!! info
    2023/12/08以降，ステージが切り替わった場合にはこの操作は`RunElecTests.sh`（後述）の中で自動的に呼び出され，ステージに応じたFE Chip Configの切り替えを行います．

モジュールをcooloing boxに設置後、解析ディレクトリーに移動.

```
$cd /nas/daq/QCtest/module-qc-operator
```

解析ディレクトリー上にある`setup.sh`を実行。

```
$ ./setup.sh -c configs/setup/repicdaq1_setup.json　
# json fileはDAQ毎に分けられている 
# DAQ1 : repicdaq1, 
# DAQ2 : repicdaq2, 
# DAQ3 : repicdaq3
```

ここで引数`-c config/setup/repicdaq1.json`はDAQのhostnameに対応したものをそれぞれ選択。  
実行後環境設定を変更するか聞かれるのでyを選択しmoudleSN, Config Typeをそれぞれ選択。  

```
> Do you want to change env setting ? (y/n)
y
Input module SN (default:20UPGM22601029 ) : 
> set moduleSN : 20UPGM22601029
```

* 環境設定は`env/.repicdaq1_env`に書き換えられる。

### Update to 1280 config

!!! info
    2023/12/12以降，このスクリプトは`RunElecTests.sh`に統合されました．
    ユーザが手でこのスクリプトを走らせる必要は(ほとんど)ありません．

2023/11/24に行われたシステムのアップデート以前のconfigは、現在のシステムでは利用できません。新しくステージを移って試験を始める際、その前のステージで最後に行われたスキャンが上記日付以前であった場合は、解析ディレクトリ

```
$cd /nas/daq/QCtest/module-qc-operator
```

にて、

```
$./lib/UpdateTo1280.sh
```

を行うと、環境変数に設定されているモジュールのconfigが新システムに向けたものにアップデートされます。

!!!warning
    以前までのtuning結果などを引き継ぐ必要があるため、新しくconfigを作るのではなく、必ずこのスクリプトで以前のconfigをアップデートすること。

### CML Biasのデフォルト値の変更(必要な場合のみ)

CML Biasのデフォルト値は、上記手順を適切に行うと、`CmlBias0=800, CmlBias1=400, CmlBias2=0`となります。EyeDiagram-based CmlBias Scanでこのデフォルト値より最適な値が見えており、かつdigital scan, analog scanなどで縞が見えた場合は、このデフォルト値の変更を検討しましょう。変更する場合は、

```
$ ./lib/ChangeCmlBias.sh [chipid] [BiasChannel [0,1,2]]  [NewValue]
```

を行います．`CmlBias1`を`200`に設定する場合は，

```
$ ./lib/ChangeCmlBias.sh all 1 200
```

とします．chip毎に個別したい場合は 一つ目の引数にchip番号を使います。(例、chip1の `CmlBias1`を`200`にする)

```
$ ./lib/ChangeCmlBias.sh 1 1 200
```

### Leakage Current Compensation (LCC)

HPKセンサのモジュールをtuningする場合に，Leakage Current CompensationのモードがONになっていないとFE1, FE4のワイヤボンド側のエッジでTuningが不良となるケースが見られています．これを緩和するために，Leakage Current Compensationという機能を用いると良いと言われており，`RunElecTests.sh`では自動的にこのモードをONにします（ただし，移行期の措置として，前のステージからTuned configを引き継ぐステージを除く）．対応するスクリプトは`lib/ModifyLccEnable.sh`です．

### Config履歴の管理

`RunElecTests.sh`でMHT/TUN/PFAのいずれかのYARR Scanのセットを実行するとき，Scanの実行に伴って，tuning parameterやマスクなどのFE Chip configの情報が更新されていきます．ここで，その途中のScanでデータ通信に不良などがあり，artificialなマスクパターンが生じてしまったとしましょう．大抵の場合，再度Scanを実行すればこのようなパターンは消えますが，すぐ前の不良なScanが作ったマスクパターンを引き継いでしまうと良好なScanをやっても好ましくないマスクが出来上がってしまいます．これを防ぐために，`RunElecTests.sh`ではMHT/TUN/PFA実行中には，Scanの履歴を保持しており，いまやったScanを繰り返す，あるいはそれ以前にやったScanに巻き戻す，という場合にはFE chip configを，該当するScanの「直前の状態」に戻す，ということを行います．`RunElecTests.sh`を一旦抜けてしまうとこの履歴は残らないため，`RunElecTests.sh`の中で繰り返しや戻る
