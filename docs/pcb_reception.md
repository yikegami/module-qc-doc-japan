# PCB Reception Test

[spread]: https://docs.google.com/spreadsheets/d/1BrP1ZUhfLx81iy4winwhAItocJb6Eg7z8EUW_kOVk5g/edit#gid=660949188
[indicatormove]: https://cernbox.cern.ch/s/oMbaXk1gicg9FTQ

KEKで全作業を行う。

PCBはSMD(抵抗などの各パーツ)の実装前後でRECEPTION PCB,POPULATIED PCBに大別され、それぞれで測定を行う。

作業工程は以下の通り。この順番で各測定を行う。

1. RECEPTION PCB Metrology
1. POPULATIED PCB Electrical Test
1. POPULATIED PCB Metrology

作業1と3はMetology Machine setupを用いる。
![KEKMetrologysetup](images/Metrologymachine_atKEK.png){align=right,width=150}




## RECEPTION PCB Metrology

### 測定項目
* Photo scan(表面)
* Size scan
* Pattern Scan
* Photo scan(裏面)

### 測定方法

1. RECEPTION PCBをMetrology Machineに設置する。
このときPCB Metrology 用の治具を事前に設置しておくこと。
![FlexSetting](images/FlexSetting.jpeg){align=right,width=150}
アライメントピンは[写真][FlexSetting]のように右上と左下のホールに刺す。(左上と右下のホールはSize scanに用いるためできるだけ傷つけたくない)

1. Metrology Machine GUIを起動し、PCBのシリアル番号を入力。PARTS、STAGE、TYPEを選択する。
    * PARTS : PCB
    * STAGE : RECEPTION
    * TYPE : PreProduction

    PCBのシリアル番号はPCB自身に記載されている番号とATLAS SNを[スプレッドシート][spread]から参照して入力する。PCB自身の番号は下の写真の赤枠で囲っている位置から参照できる。
    ![FlexSNposition](images/FlexSNposition.jpeg){align=right,width=150}

1. *Set Scan list*のボタンを押してScan項目を確認する。基本的には全てcheckがついた状態でOKを選択する。

1. 緑のStartボタンを押す。画面の指示に従いscanを行う。基本的にはscanは全て自動的に進行し、startボタンを押した後は基本的にこちらから別のscanを指定する必要はない。適宜ポップアップウインドウが出るのでその指示に従うこと。

1. 最初にphoto scanが始まり、撮影完了後、Flex全体の合成写真のウインドウがポップアップする。写真が正常に合成されていることを確認できたらウインドウを消す。その後「*"Do you want to save this data to disk server as background process?"*」と聞かれるので正しく測定できていれば「Yes」を選択。
合成された写真データがdisk serverにuploadされる。

1. 「Yes」を選択後、自動でSize scanが動作する。scan終了後、下の写真のFidusial Markにフォーカスが合っているかどうかを確認し、photo scanと同様の操作でdiskにuploadする。
 ![FlexFidusialMark](images/FlexFidusialMark.png){align=right,width=150}

 1. 次にFlex pattern scanを行う。この測定にはインジケータと呼ばれる装置を使う。これはMetrology Machineの左横に設置されている。測定の様子は[リンク][indicatormove]を参照。
     ![indicator](images/indicator.jpeg){align=right,width=150}

    #### インジケータ使用方法

    1. 基本的にはポップアップの指示に従う。
    インジケータの横のメモリがEvacuate positionにあるかを確認。
     ![indicatorposition](images/indicatorPosition.jpeg){align=right,width=150}

    1. ポップアップの指示が出たらインジケータの位置を横のつまみを回して'Measurement' positionに変更する。

    1. 測定終了後、Evacuate positionに戻すようにポップアップが再び出るのでつまみを回してインジケータの位置を戻す。


1. 次は裏面のphoto scanをおこなう。画面の指示に従い、右隣の治具を用いてPCBをひっくり返す。
1. 表面と同様、測定完了後に合成された写真がポップアップする。正常な写真であればウインドウを閉じ、ポップアップの指示に従う。
1. PCBを表面に戻す。
1. 自動でMachineがreplace positionに移動する。PCBを保管して測定完了。
1. [スプレッドシート][spread]のYamashita PCB v4.1 QC のシート上に測定結果が保存されているディレクトリ名を後で解析するために記録する。


## POPULATIED PCB Electrical Test
Assembled FlexのMetrologyの後に行う。

### 測定手順
1. Flexを袋から取り出してpig tailのコネクタを外す
1. ベース基板のアライメントピンに沿ってFlexを取り付ける。
flat cableコネクタが上に来る向きで設置。
![FlexElectricaltest1](images/Electricaltest_set.png){align=right,width=150}
![FlexElectricaltest2](images/Electricaltest_set2.jpeg){align=right,width=150}
1. 上のケーブルがついた基板を取り付ける。
Flexが通電してるとpico logのNTCの電圧が1.2V前後になっている。(通電していないと200mVとか)
このとき、ワニの部分をHVのコネクタにつけることを忘れないこと!
![FlexElectricaltest3](images/Electricaltest_set3.jpeg){align=right,width=150}
1. PC上のpico logに移動し、前の測定結果をreset
2. 録画の赤い丸のボタンを押してlocalで記録する。
測定時間を **11分** に設定して測定開始
3. グラフのボタンを押してHV LVをモニターできるようにする。
4. その後HVLVの電源を入れて測定開始。
1. PC上でタイマーをセット。
5. GND+(赤)とGND-(青),VIN+(緑)とVIN-(黄色)の電圧ドロップをみる。この間にCC 5Aがかかっており、電圧がおよそ400mV dropする。
このとき、picologに接続しているアンプで電圧を10倍にしているので 400/10 = 40 mV
よってこの間の抵抗は 0.04/5 = 8mΩ
6. 同様にVIN+とVIN-の電圧dropも観察する。ここの間の電圧dropは大体35mVで
35mV/ 5A = 7mΩ程度の抵抗がかかっている。
7. 紫のHVのlineはcapacitanceにかかっているHVで時定数がみれる。おおよそ最後のほうで100mVくらいの電圧でプラトーになればいい
![picologGraph](images/picologGraph.png){align=right,width=150}
1. 21分経過後、データの保存を行う。
    1. 切り取り&スケッチでpicolog上に表示されたグラフをスクリーンショットして保存する。
    保存名はPCBのATLAS SN、保存場所は `NAS~~/ITkPixel/PreProduction/FlexLVHV/ `
    1. picologのアプリケーションに移動し、picologの生データを保存する。
    画面上のフロッピーディスクのマークをクリックし、「データを保存」を選択。
    「参照」をクリック。先ほどと同じディレクトリに移動し、ATLAS SNをファイル名にして保存
    1. 次に隣の文書マークのボタンをクリックし、pdfとcsvをエクスポートする。
    どちらもATLAS SNのファイル名で保存。保存ディレクトリも先ほどと同様。
    1. 最後に[スプレッドシート][spread]のYamashita PCB v4.1 QCのシートの「LVHV」の列にtest resultと温度、leakage currentを記載する。温度とleakage currentは横のHV Power Supplyに表示されている値を読み取る。

1. データの保存が終わったらLVHVのoutputをoffにする。
1. PCBをQC setupから取り外してコネクタにカバーをつけ、袋に入れて保管する。その後Metrologyの測定に移る。

## POPULATIED PCB Metrology

### 測定項目
* Photo scan(表面)
* Size scan

### 測定方法
1. ハサミでPCBの入った袋の封を切る。
1. RECEPTION PCB Metrologyの時と同様にMetrology Machineに設置する。
1. Metrology Machine GUIを起動し、PCBのシリアル番号を入力し、PARTS、STAGE、TYPEを選択する。
    * PARTS : PCB
    * STAGE : POPULATION
    * TYPE : PreProduction
1. 'Set Scan list'のボタンを押してScan項目を確認する。基本的には全てcheckがついた状態でOKを選択する。
1. 緑のStartボタンを押す。画面の指示に従いscanを行う。
1. 測定が終わったらPCBのコネクタにカバーをつけ、袋に戻す。
その後口のついたジップロック系の袋に入れて保管。
1. [スプレッドシート][spread]に測定したデータのディレクトリ名を記録する。

### 参考資料
Bare Flex QC manual wrote by Mami Takagi
[https://cernbox.cern.ch/s/wJ27IG0J1Dvzpz7](https://cernbox.cern.ch/s/wJ27IG0J1Dvzpz7)

Assembled Flex QC manual wrote by Yuta Hiemori
[https://cernbox.cern.ch/pdf-viewer/public/fXoVK6zTKxn9Zjm/howtoDoFlexQC.pdf?contextRouteName=files-public-link&contextRouteParams.driveAliasAndItem=public%2FfXoVK6zTKxn9Zjm&items-per-page=100](https://cernbox.cern.ch/pdf-viewer/public/fXoVK6zTKxn9Zjm/howtoDoFlexQC.pdf?contextRouteName=files-public-link&contextRouteParams.driveAliasAndItem=public%2FfXoVK6zTKxn9Zjm&items-per-page=100)