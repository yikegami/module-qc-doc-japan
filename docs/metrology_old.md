# メトロロジー手順

[2022年版マニュアル]([Dropbox - MetrologyManualv2.pdf - 日常をシンプルに](https://www.dropbox.com/s/i1k0pgapwq0ulnt/MetrologyManualv2.pdf?dl=0)

# Bare module metrology

## モジュールの設置・取り外しの注意点

- モジュールの設置・取り外しには、GUIで"Replace position"ボタンを押して治具を手前に移動させて作業するとよい。(GUI左の青緑の長方形のボタン)
- モジュールを取り外しは真空吸着をオフにしてから行う。こちらはON,OFFをGUI上で確認できる。

## 表面の測定

- Metrology装置の治具の左上（position 1）の設置場所の周り3点に位置決めのためのピンを立てる。
- Bare moduleのピンに押し当てるようにして設置する。
- 設置後、真空吸着をオンにする。

## 裏面の測定

- Bare moduleの表裏ひっくり返してから、表面と同様の手順でピンに押し当てるようにbare moduleを設置して真空吸着をオンにする。

## スキャンの実行

- GUIの”Scan Parameters"欄の"Scan Type:"を選択する。
  - Visual inspectionまたはsize測定のそれぞれに対応するscan typeは以下の表の通りである。

Scan types for bare module:

| Purpose                        | Scan type                       | Comments                         |
| ------------------------------ | ------------------------------- | -------------------------------- |
| Visual inspection (front/back) | Pixel module Flex photo         | LED: OFF, シャッタースピード: 1/4秒        |
| Visual inspection (front)      | Pixel module Flex photo(LED ON) | LED: ON, シャッタースピード: 1/4秒, 余裕があれば |
| Size                           | Bare module size                | LED: ON, シャッタースピード: 1/4000秒      |
| thickness (front/back)         | Pixel Bare module scan (VON)    | LED: OFF, カメラOFF 20倍レンズ       |
| flatness (front/back)         | Pixel Bare module scan (VOFF)    | LED: OFF, カメラOFF 20倍レンズ       |

## 結果の確認

- Visual inspectionでは、スキャン実行後すぐに複数の写真を貼り合わせた写真が表示される。
  - 全体がちゃんと写っていればよい。確認後、この写真は閉じてしまってよい。
  - 写真を閉じると、バックグラウンドで写真を合成するかどうか聞かれるので"OK"を押す。
- サイズ測定では、以下の箇所を測定している。
  - 目的とするパターン（マークは辺などの図形）が写っていてピントが合っていることを確認する。

<details><summary>治具の表面（高さの基準）</summary>
![治具表面](http://hpx.phys.ocha.ac.jp/user/tkohno/work/ITk/Metrology/Manual/figures/ref_baremodule_jig.png)
</details>
<details><summary>ASIC左辺</summary>
![ASIC左辺](http://hpx.phys.ocha.ac.jp/user/tkohno/work/ITk/Metrology/Manual/figures/ref_baremodule_asic_left.png)
</details>
<details><summary>ASIC右辺</summary>
![ASIC右辺](http://hpx.phys.ocha.ac.jp/user/tkohno/work/ITk/Metrology/Manual/figures/ref_baremodule_asic_right.png)
</details>
<details><summary>Sensor下辺</summary>
![Sensor下辺](http://hpx.phys.ocha.ac.jp/user/tkohno/work/ITk/Metrology/Manual/figures/ref_baremodule_sensor_bottom.png)
</details>
<details><summary>Sensor上辺</summary>
![Sensor上辺](http://hpx.phys.ocha.ac.jp/user/tkohno/work/ITk/Metrology/Manual/figures/ref_baremodule_sensor_top.png)
</details>
<details open="true"><summary>Sensor表面</summary>
![Sensor表面](http://hpx.phys.ocha.ac.jp/user/tkohno/work/ITk/Metrology/Manual/figures/ref_baremodule_inside.png)
</details>

# PCB Metrology

# Reception test: Flex PCB

## モジュールの設置と取り外し

- モジュールの設置・取り外しには、GUIで"Replace"ボタンを押して治具を手前に移動させて作業するとよい。
- モジュールを取り外しは真空吸着をオフにしてから行う。

## 表面の測定

- Metrology装置の治具の左上（position 1）の設置場所にFlexを設置する。
  - 位置決めのため、Flexの周囲にある枠の穴にピンを挿して治具に固定する。
  - ピンは右上と左下の穴に挿すこと。左上と右下の穴径を測る必要があるのでこれらは塞がないように注意すること。
- 設置後、真空吸着をオンにする。

## 裏面の測定

- Metrology装置の治具の右上（position 3）の設置場所にFlexを設置する。
  - 一度、表面の測定の手順によってposition 1で位置決めをする。
  - 真空吸着をオンにした後、位置決めのために取り付けていたンを外す。
  - 右側の治具を左側の治具に被せるようにして近づけていき、Position 3の4つの真空吸着がFlex表面に触れたら真空吸着を下側の治具から上側の治具に切り替える。
  - 上側の治具（position 3）で真空吸着できたら、そのまま上側の治具を持ち上げて元の位置に戻す。
- Position 3の中心を原点にする。
  - GUIでPosition 3を選択して、ステージを移動させる。
  - GUIで"Set current position as Original Position"ボタンを押す。（原点として設定）
- 裏面測定ではメニューを選択後、再度position 3で原点として設定する必要がある。

## スキャンの実行

- GUIの”Scan Parameters"欄の"Scan Type:"を選択する。
  - Visual inspectionまたはsize測定のそれぞれに対応するscan typeは以下の表の通りである。

Scan types for v4.1 Flex:
| Purpose | Scan type | Comments |
| --- | --- | --- |
| Visual inspection (front/back) | Pixel module Flex extended photo | LED: OFF, シャッタースピード: 1/4秒 |
| Size | Pixel Flex size ITkpixv4.1 | LED: ON, シャッタースピード: 1/4000秒 |

Scan types for v3.2 Flex:
| Purpose | Scan type | Comments |
| --- | --- | --- |
| Visual inspection (front/back) | Pixel module Flex extended photo | LED: OFF, シャッタースピード: 1/4秒 |
| Size | Pixel Flex size ITkpix | LED: ON, シャッタースピード: 1/4000秒 |

## 結果の確認

- Visual inspectionでは、スキャン実行後すぐに複数の写真を貼り合わせた写真が表示される。
  - 全体がちゃんと写っていればよい。確認後、この写真は閉じてしまってよい。
- サイズ測定では、以下の箇所を測定している。
  - 目的とするパターン（マークは辺などの図形）が写っていてピントが合っていることを確認する。

<details><summary>治具の表面（高さの基準）</summary>
![治具表面](http://hpx.phys.ocha.ac.jp/user/tkohno/work/ITk/Metrology/Manual/figures/ref_flex_jig.png)
</details>
<details><summary>アラインメント・ホール</summary>
![治具表面](http://hpx.phys.ocha.ac.jp/user/tkohno/work/ITk/Metrology/Manual/figures/ref_flex_holes.png)
</details>
<details><summary>金パッド（高さ）</summary>
![金パッド](http://hpx.phys.ocha.ac.jp/user/tkohno/work/ITk/Metrology/Manual/figures/ref_flex_pads.png)
</details>
<details><summary>Fiducial marks</summary>
![Fiducial marks](http://hpx.phys.ocha.ac.jp/user/tkohno/work/ITk/Metrology/Manual/figures/ref_flex_fmarks.png)
</details>
<details><summary>HV capacitor（高さ）</summary>
![HV capacitor](http://hpx.phys.ocha.ac.jp/user/tkohno/work/ITk/Metrology/Manual/figures/ref_flex_hvcapacitor.png)
</details>
<details><summary>Power connector（高さ）</summary>
![Power connector](http://hpx.phys.ocha.ac.jp/user/tkohno/work/ITk/Metrology/Manual/figures/ref_flex_connector.png)
</details>
<details><summary>左辺</summary>
![Left edge](http://hpx.phys.ocha.ac.jp/user/tkohno/work/ITk/Metrology/Manual/figures/ref_flex_left.png)
</details>
<details><summary>下辺</summary>
![Bottom edge](http://hpx.phys.ocha.ac.jp/user/tkohno/work/ITk/Metrology/Manual/figures/ref_flex_bottom.png)
</details>
<details><summary>右辺</summary>
![Right edge](http://hpx.phys.ocha.ac.jp/user/tkohno/work/ITk/Metrology/Manual/figures/ref_flex_right.png)
</details>
<details open="true"><summary>上辺</summary>
![Top edge](http://hpx.phys.ocha.ac.jp/user/tkohno/work/ITk/Metrology/Manual/figures/ref_flex_top.png)
</details>

# 組み立て後のモジュールの形状測定 (Stage: MODULE/ASSEMBLY)

## モジュールの設置・取り外しの注意点

- モジュールの設置・取り外しには、GUIで"Replace"ボタンを押して治具を手前に移動させて作業するとよい。
- モジュールを取り外しは真空吸着をオフにしてから行う。

## 表面の測定

- Metrology装置の治具の左上（position 1）の設置場所にFlexを設置する。
  - 位置決めのため、Flexの周囲にある枠の穴にピンを挿して治具に固定する。
  - ピンは右上と左下の穴に挿すこと。左上と右下の穴径を測る必要があるのでこれらは塞がないように注意すること。
- 設置後、真空吸着をオンにする。

## 裏面の測定

- Metrology装置の治具の右上（position 3）の設置場所にFlexを設置する。
  - 一度、表面の測定の手順によってposition 1で位置決めをする。
  - 真空吸着をオンにした後、位置決めのために取り付けていたンを外す。
  - 右側の治具を左側の治具に被せるようにして近づけていき、Position 3の4つの真空吸着がFlex表面に触れたら真空吸着を下側の治具から上側の治具に切り替える。
  - 上側の治具（position 3）で真空吸着できたら、そのまま上側の治具を持ち上げて元の位置に戻す。
- Position 3の中心を原点にする。
  - GUIでPosition 3を選択して、ステージを移動させる。
  - GUIで"Set current position as Original Position"ボタンを押す。（原点として設定）
- 裏面測定ではメニューを選択後、再度position 3で原点として設定する必要がある。

## スキャンの実行

- GUIの”Scan Parameters"欄の"Scan Type:"を選択する。
  - Visual inspectionまたはsize測定のそれぞれに対応するscan typeは以下の表の通りである。

Scan types for bare module:

| Purpose                        | Scan type                       | Comments                         |
| ------------------------------ | ------------------------------- | -------------------------------- |
| Visual inspection (front/back) | Pixel module Flex photo         | LED: OFF, シャッタースピード: 1/4秒        |
| Visual inspection (front)      | Pixel module Flex photo(LED ON) | LED: ON, シャッタースピード: 1/4秒, 余裕があれば |
| Size                           | ITkpixv1 module size            | LED: ON, シャッタースピード: 1/4000秒      |

## 結果の確認

- Visual inspectionでは、スキャン実行後すぐに複数の写真を貼り合わせた写真が表示される。
  - 全体がちゃんと写っていればよい。確認後、この写真は閉じてしまってよい。
  - 写真を閉じると、バックグラウンドで写真を合成するかどうか聞かれるので"OK"を押す。
- サイズ測定では、以下の箇所を測定している。
  - 目的とするパターン（マークは辺などの図形）が写っていてピントが合っていることを確認する。

<details><summary>治具の表面（高さの基準）</summary>
![治具表面](http://hpx.phys.ocha.ac.jp/user/tkohno/work/ITk/Metrology/Manual/figures/ref_module_jig.png)
</details>
<details><summary>Flex pickup areas</summary>
![Flex pickup area](http://hpx.phys.ocha.ac.jp/user/tkohno/work/ITk/Metrology/Manual/figures/ref_module_flex_pickup.png)
</details>
<details><summary>Flex fiducial marks</summary>
![Flex fiducial marks](http://hpx.phys.ocha.ac.jp/user/tkohno/work/ITk/Metrology/Manual/figures/ref_module_flex_fmark.png)
</details>
<details><summary>ASIC fiducial marks</summary>
![ASIC fiducial marks](http://hpx.phys.ocha.ac.jp/user/tkohno/work/ITk/Metrology/Manual/figures/ref_module_asic_fmark.png)
</details>
<details><summary>HV capacitor</summary>
![HV capacitor](http://hpx.phys.ocha.ac.jp/user/tkohno/work/ITk/Metrology/Manual/figures/ref_module_hvcapacitor.png)
</details>
<details><summary>Powerコネクタ</summary>
![Powerコネクタ](http://hpx.phys.ocha.ac.jp/user/tkohno/work/ITk/Metrology/Manual/figures/ref_module_connector.png)
</details>
<details><summary>ASIC右辺</summary>
![ASIC右辺](http://hpx.phys.ocha.ac.jp/user/tkohno/work/ITk/Metrology/Manual/figures/ref_module_asic_right.png)
</details>
<details><summary>Sensor下辺</summary>
![Sensor下辺](http://hpx.phys.ocha.ac.jp/user/tkohno/work/ITk/Metrology/Manual/figures/ref_module_sensor_bottom.png)
</details>
<details><summary>ASIC左辺</summary>
![ASIC左辺](http://hpx.phys.ocha.ac.jp/user/tkohno/work/ITk/Metrology/Manual/figures/ref_module_asic_left.png)
</details>
<details><summary>Sensor上辺</summary>
![Sensor上辺](http://hpx.phys.ocha.ac.jp/user/tkohno/work/ITk/Metrology/Manual/figures/ref_module_sensor_top.png)
</details>
<details><summary>Flex左辺</summary>
![Flex左辺](http://hpx.phys.ocha.ac.jp/user/tkohno/work/ITk/Metrology/Manual/figures/ref_module_flex_left.png)
</details>
<details><summary>Flex上辺</summary>
![Flex上辺](http://hpx.phys.ocha.ac.jp/user/tkohno/work/ITk/Metrology/Manual/figures/ref_module_flex_top.png)
</details>
<details><summary>Flex右辺</summary>
![Flex右辺](http://hpx.phys.ocha.ac.jp/user/tkohno/work/ITk/Metrology/Manual/figures/ref_module_flex_right.png)
</details>
<details open="true"><summary>Flex下辺</summary>
![Flex下辺](http://hpx.phys.ocha.ac.jp/user/tkohno/work/ITk/Metrology/Manual/figures/ref_module_flex_bottom.png)
</details>
