# シリアル番号の管理

* [日本クラスタのGoogle Spreadsheet](https://docs.google.com/spreadsheets/d/1BrP1ZUhfLx81iy4winwhAItocJb6Eg7z8EUW_kOVk5g/edit?usp=sharing) 
* [シリアル番号生成スクリプト](https://gitlab.cern.ch/atlas-jp-itk/jp-itkpd-scripts.git)

## BareModuleの生成

BareModuleを生成するための，いくつかのオリジナルな情報がSpreadsheetに入力される．

Pre-productionでは"Preproduction module"というタブを主に参照する．

1. Batch ID
    HPKから送られてくるBareModuleのパッケージごとにincrementする．

2. センサーID
   
   * e.g. `S16393-2299 w10-2` から "alternative name"と呼ばれるNicknameを生成する．
   * alternative nameを用いて，ITkPDから取得する各sensor tileのSNのリストから検索することで，Sensor SNを決定し，スプレッドシートを埋める．これは`query_sensor_sn.py`で行う．

3. Sensor Wafer SN
   
   * e.g. `0x159`.
   * ASIC Id: e.g. `2-7` : wafer上のFEの座標（行・列）
   * この２つを組み合わせるとFE chip SN `20U PG FC xxxxxxx`が生成できる．

4. Sensor SNとFE Chip SNが全て決まれば，BareModuleを生成できる．すべてのSensor SNとFE Chip SNのcurrentLocationはITkPD上で，HPKに指定されている必要がある．これが満たされていない場合は，それぞれのコンポーネント，あるいはその親のWaferのcurrentLocationをHPKに移動することが必要．この条件で，`register_bare_modules.py`で生成できる．

5. BareModuleをKEKを経由してHRに送る．`ship_bare_modules_HPK_to_KEK.py`と`ship_bare_modules_KEK_to_HR.py`の２つのスクリプトが準備．

## PCBの生成

PCBのSNは「Yamashita PCB v4.1 QC」のタブから取れる．`register_pcbs.py`を用いて登録する．
`ship_pcbs_KEK_to_HR.py`を用いてHRに送る．

## Moduleの生成

BareModuleとPCBを組み合わせてModuleを生成する．BareModuleとPCBの両方のcurrentLocationがHRになっていることが必要．Module SNの下６ケタはPCBの下６ケタと一致させるため，この生成作業はLocalDBで行う．

## ProductionDBからLocalDBへSN情報を渡す

Pull componentからModule SNを指定することでLocalDB上でQC作業を始めることができる．
Stage coherencyのため，BareModuleやPCBのQCはアセンブリより前に終えておくことが本来必要．

## ラベルの生成

* ソフトウェア[「TEPRA LINK2」(macOS)](https://www.kingjim.co.jp/sp/tepra_link2/)または[「TEPRA Label Editor SPC10」](https://www.kingjim.co.jp/download/tepra/labeleditor.html)を使用します．
* TEPRA SR-R7900PはLANでネットワーク全体から見えています．
* テープはKigjim SC18YV（幅は18mm，黄色，屋外に強いラベル）を使用．
* テンプレートはソフトウェア上の「ラベルデザイン」から．「整理収納」カテゴリを選択し，「QRコード」のついた「備品管理①」を使用します．以下の図の通りに間違いのないように記入します．

![TEPRA LINK](images/tepra_link_1.png)
![TEPRA LINK](images/tepra_link_2.png)

* QRコードをダブルクリックすると内容を編集できるので，「モジュールのシリアル番号」を入力します．
* __間違いのないことをもう一度確認します．__
* ラベルを印刷し，キャリアのカバーに貼ります．

* windows用TEPRAテンプレート
![TEPRA WINDOWS TEMP] (https://cernbox.cern.ch/s/VKTlq2EuIwnJwlf)
